"use strict";

var _interopRequireWildcard3 = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _interopRequireWildcard2 = _interopRequireDefault(require("@babel/runtime/helpers/interopRequireWildcard"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireWildcard3(require("react"));

var _reactSelect = _interopRequireDefault(require("react-select"));

var _InputLabel = _interopRequireDefault(require("../FormElements/InputLabel"));

var _default = function _default(_ref) {
  var field = _ref.field,
      value = _ref.value,
      validationMessage = _ref.validationMessage,
      touched = _ref.touched,
      setTouched = _ref.setTouched,
      hideField = _ref.hideField,
      updateForm = _ref.updateForm,
      styledComponents = _ref.styledComponents,
      error = _ref.error,
      unsetError = _ref.unsetError,
      setFocusClass = _ref.setFocusClass,
      cssClass = _ref.cssClass,
      language = _ref.language,
      props = (0, _objectWithoutProperties2["default"])(_ref, ["field", "value", "validationMessage", "touched", "setTouched", "hideField", "updateForm", "styledComponents", "error", "unsetError", "setFocusClass", "cssClass", "language"]);
  var id = field.id,
      formId = field.formId,
      type = field.type,
      label = field.label,
      placeholder = field.placeholder,
      isRequired = field.isRequired,
      inputs = field.inputs,
      maxLength = field.maxLength,
      description = field.description,
      descriptionPlacement = field.descriptionPlacement,
      labelPlacement = field.labelPlacement,
      width = field.width,
      customName = field.customName;

  var _useState = (0, _react.useState)([]),
      _useState2 = (0, _slicedToArray2["default"])(_useState, 2),
      countryNames = _useState2[0],
      setNames = _useState2[1];

  function getCountries() {
    Promise.resolve().then(function () {
      return (0, _interopRequireWildcard2["default"])(require("i18n-iso-countries"));
    }).then(function (countries) {
      countries.registerLocale(require("i18n-iso-countries/langs/".concat(language ? language : "en", ".json")));
      var names = Object.values(countries.getNames(language ? language : "en", {
        select: "official"
      })).map(function (a) {
        return a;
      }).sort(function (a, b) {
        return a.localeCompare(b);
      });
      console.log("names", names);
      setNames(names);
    })["catch"](function (error) {
      return console.log(error);
    });
  }

  var _ref2 = styledComponents || false,
      _ref2$Input = _ref2.Input,
      Input = _ref2$Input === void 0 ? "input" : _ref2$Input,
      _ref2$Label = _ref2.Label,
      Label = _ref2$Label === void 0 ? "label" : _ref2$Label,
      _ref2$Box = _ref2.Box,
      Box = _ref2$Box === void 0 ? "div" : _ref2$Box,
      ReactSelect = _ref2.ReactSelect;

  var RSelect = ReactSelect || _reactSelect["default"];

  var handleChange = function handleChange(option) {
    var event = option && option.target ? option : {
      target: {
        value: option
      }
    };
    updateForm(event, field);
  };

  (0, _react.useEffect)(function () {
    getCountries();
  }, []);
  return _react["default"].createElement(Box, {
    width: width,
    className: validationMessage && touched || error ? "form-field error ".concat(cssClass) : "form-field ".concat(cssClass),
    style: {
      display: hideField ? "none" : undefined
    }
  }, inputs === null || inputs === void 0 ? void 0 : inputs.map(function (input, key) {
    return !input.isHidden && _react["default"].createElement("div", {
      className: type,
      key: input.id
    }, _react["default"].createElement(_InputLabel["default"], {
      formId: formId,
      id: input.id,
      label: input.label,
      labelPlacement: labelPlacement,
      isRequired: isRequired,
      styledComponent: styledComponents
    }), descriptionPlacement === "above" && description && _react["default"].createElement("div", {
      className: "description"
    }, description), key === 5 && countryNames ? _react["default"].createElement(RSelect, {
      onChange: function onChange(event) {
        handleChange(event);
        unsetError(input.id);
      },
      onBlur: function onBlur(event) {
        updateForm(event, field);
        setTouched(input.id);
        setFocusClass(value !== "");
      },
      onFocus: function onFocus() {
        return setFocusClass(true);
      },
      options: countryNames.map(function (item) {
        return {
          label: item,
          value: item
        };
      })
    }, countryNames.map(function (country) {
      return _react["default"].createElement("option", {
        value: country
      }, country);
    })) : _react["default"].createElement(Input, {
      id: "input_".concat(formId, "_").concat(input.id),
      key: input.id,
      name: customName || "input_".concat(input.id),
      type: type,
      value: !value ? "" : value[input.id],
      placeholder: input.placeholder,
      maxLength: maxLength,
      required: isRequired,
      onChange: function onChange(event) {
        updateForm(event, field, input.id);
        unsetError(input.id);
      },
      onBlur: function onBlur(event) {
        updateForm(event, field);
        setTouched(input.id);
        setFocusClass(value !== "");
      },
      onFocus: function onFocus() {
        return setFocusClass(true);
      },
      "aria-label": input.label,
      "aria-describedby": "error_".concat(formId, "_").concat(input.id),
      "aria-invalid": !!validationMessage && touched || !!error
    }), descriptionPlacement !== "above" && description && _react["default"].createElement("div", {
      className: "description"
    }, description), (validationMessage && touched || error) && _react["default"].createElement("span", {
      className: "error-message",
      id: "error_".concat(formId, "_").concat(id)
    }, validationMessage || error));
  }));
};

exports["default"] = _default;