"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireWildcard(require("react"));

var _reactSelect = _interopRequireDefault(require("react-select"));

var _InputLabel = _interopRequireDefault(require("../FormElements/InputLabel"));

var _default = function _default(_ref) {
  var field = _ref.field,
      value = _ref.value,
      validationMessage = _ref.validationMessage,
      touched = _ref.touched,
      setTouched = _ref.setTouched,
      setFocusClass = _ref.setFocusClass,
      hideField = _ref.hideField,
      updateForm = _ref.updateForm,
      styledComponents = _ref.styledComponents,
      error = _ref.error,
      cssClass = _ref.cssClass,
      unsetError = _ref.unsetError,
      props = (0, _objectWithoutProperties2["default"])(_ref, ["field", "value", "validationMessage", "touched", "setTouched", "setFocusClass", "hideField", "updateForm", "styledComponents", "error", "cssClass", "unsetError"]);
  var id = field.id,
      formId = field.formId,
      type = field.type,
      label = field.label,
      isRequired = field.isRequired,
      choices = field.choices,
      placeholder = field.placeholder,
      description = field.description,
      descriptionPlacement = field.descriptionPlacement,
      labelPlacement = field.labelPlacement,
      width = field.width,
      customName = field.customName;
  var selected = '';
  var options = choices.map(function (choice) {
    var item = {
      value: choice.value,
      label: choice.text
    };

    if (choice.isSelected) {
      selected = item;
    }

    return item;
  });

  var _useState = (0, _react.useState)(value || selected),
      _useState2 = (0, _slicedToArray2["default"])(_useState, 2),
      selectedOption = _useState2[0],
      selectOption = _useState2[1];

  var handleChange = function handleChange(option) {
    selectOption(option);
    var event = {
      target: {
        value: option
      }
    };
    updateForm(event, field);
  };

  var handleBlur = function handleBlur() {
    var event = {
      target: {
        value: selectedOption
      }
    };
    updateForm(event, field);
    setTouched(id);
    setFocusClass(selectedOption && selectedOption.value);
  };

  var _ref2 = styledComponents || false,
      ReactSelect = _ref2.ReactSelect,
      _ref2$Label = _ref2.Label,
      Label = _ref2$Label === void 0 ? 'label' : _ref2$Label,
      _ref2$Box = _ref2.Box,
      Box = _ref2$Box === void 0 ? 'div' : _ref2$Box;

  var RSelect = ReactSelect || _reactSelect["default"];
  return _react["default"].createElement(Box, {
    width: width,
    className: validationMessage && touched || error ? "form-field error ".concat(cssClass) : "form-field ".concat(cssClass),
    style: {
      display: hideField ? 'none' : undefined
    }
  }, _react["default"].createElement("div", {
    className: type
  }, _react["default"].createElement(_InputLabel["default"], {
    formId: formId,
    id: id,
    label: label,
    labelPlacement: labelPlacement,
    isRequired: isRequired,
    styledComponent: styledComponents
  }), descriptionPlacement === 'above' && description && _react["default"].createElement("div", {
    className: "description"
  }, description), _react["default"].createElement(RSelect, {
    name: customName || "input_".concat(id),
    required: isRequired,
    value: selectedOption && selectedOption.value ? selectedOption : '',
    onChange: function onChange(option) {
      handleChange(option, field);
      unsetError(id);
    },
    onBlur: function onBlur() {
      return handleBlur();
    },
    onFocus: function onFocus() {
      return setFocusClass(true);
    },
    placeholder: placeholder,
    options: options,
    className: "form-select",
    autoFocus: false,
    inputId: "input_".concat(formId, "_").concat(id)
  }), descriptionPlacement !== 'above' && description && _react["default"].createElement("div", {
    className: "description"
  }, description), (validationMessage && touched || error) && _react["default"].createElement("span", {
    className: "error-message",
    id: "error_".concat(formId, "_").concat(id)
  }, validationMessage || error)));
};

exports["default"] = _default;