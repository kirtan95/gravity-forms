"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireDefault(require("react"));

var _default = function _default(_ref) {
  var field = _ref.field,
      styledComponents = _ref.styledComponents,
      component = _ref.component,
      props = (0, _objectWithoutProperties2["default"])(_ref, ["field", "styledComponents", "component"]);
  var cssClass = field.cssClass,
      width = field.width;

  var _ref2 = styledComponents || false,
      _ref2$Box = _ref2.Box,
      Box = _ref2$Box === void 0 ? 'div' : _ref2$Box;

  var Component = component;
  return Component ? _react["default"].createElement(Box, {
    width: width,
    className: "form-field ".concat(cssClass)
  }, _react["default"].createElement(Component, (0, _extends2["default"])({
    field: field,
    styledComponents: styledComponents
  }, props))) : '';
};

exports["default"] = _default;