"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireWildcard(require("react"));

var _DatePicker = _interopRequireDefault(require("./DatePicker"));

var _DateSelect = _interopRequireDefault(require("./DateSelect"));

var _DateInput = _interopRequireDefault(require("./DateInput"));

var _InputLabel = _interopRequireDefault(require("../../FormElements/InputLabel"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var _default = function _default(_ref) {
  var field = _ref.field,
      value = _ref.value,
      validationMessage = _ref.validationMessage,
      touched = _ref.touched,
      setTouched = _ref.setTouched,
      hideField = _ref.hideField,
      updateForm = _ref.updateForm,
      styledComponents = _ref.styledComponents,
      error = _ref.error,
      unsetError = _ref.unsetError,
      setFocusClass = _ref.setFocusClass,
      cssClass = _ref.cssClass,
      props = (0, _objectWithoutProperties2["default"])(_ref, ["field", "value", "validationMessage", "touched", "setTouched", "hideField", "updateForm", "styledComponents", "error", "unsetError", "setFocusClass", "cssClass"]);
  var id = field.id,
      formId = field.formId,
      type = field.type,
      label = field.label,
      isRequired = field.isRequired,
      description = field.description,
      descriptionPlacement = field.descriptionPlacement,
      labelPlacement = field.labelPlacement,
      width = field.width,
      inputs = field.inputs,
      dateType = field.dateType,
      dateFormat = field.dateFormat,
      defaultValue = field.defaultValue;

  var _ref2 = styledComponents || false,
      _ref2$Box = _ref2.Box,
      Box = _ref2$Box === void 0 ? 'div' : _ref2$Box;

  var format = dateFormat && dateFormat === 'dmy' ? 'dd/MM/yyyy' : false;
  var selectedValue = defaultValue ? new Date(defaultValue) : false;

  if (format && defaultValue) {
    var tmpFormat = defaultValue.indexOf('/') > -1;
    var dateParts = tmpFormat ? defaultValue.split('/') : defaultValue.split('-');
    var dateObject = tmpFormat ? new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]) : dateParts;
    selectedValue = dateObject;
  }

  var _useState = (0, _react.useState)(selectedValue),
      _useState2 = (0, _slicedToArray2["default"])(_useState, 2),
      startDate = _useState2[0],
      setDate = _useState2[1];

  var getFormattedInputs = function getFormattedInputs(items) {
    if (dateType) {
      if (dateType === 'datefield') {
        switch (dateFormat) {
          case 'dmy':
          case 'dmy_dash':
          case 'dmy_dot':
            return [items[1], items[0], items[2]];

          case 'ymd_slash':
          case 'ymd_dash':
          case 'ymd_dot':
            return [items[2], items[0], items[1]];

          default:
            return items;
        }
      } else if (dateType === 'datedropdown') {
        return [items[1], items[0], items[2]];
      }
    }

    return items;
  };

  var formattedInputs = getFormattedInputs(inputs);

  var defaultProps = _objectSpread({
    id: id,
    field: field,
    isRequired: isRequired,
    format: format,
    formattedInputs: formattedInputs,
    updateForm: updateForm,
    startDate: startDate,
    setTouched: setTouched,
    setDate: setDate,
    setFocusClass: setFocusClass,
    error: error,
    unsetError: unsetError,
    validationMessage: validationMessage,
    styledComponents: styledComponents,
    touched: touched
  }, props);

  var renderDateField = function renderDateField(dateType) {
    switch (dateType) {
      case 'datepicker':
        return _react["default"].createElement(_DatePicker["default"], {
          defaultProps: defaultProps
        });

      case 'datedropdown':
        return _react["default"].createElement(_DateSelect["default"], {
          defaultProps: defaultProps
        });

      default:
        return _react["default"].createElement(_DateInput["default"], {
          defaultProps: defaultProps
        });
    }
  };

  return _react["default"].createElement(Box, {
    width: width,
    className: validationMessage && touched || error ? "form-field error ".concat(cssClass) : "form-field ".concat(cssClass),
    style: {
      display: hideField ? 'none' : undefined
    }
  }, _react["default"].createElement(_InputLabel["default"], {
    formId: formId,
    id: id,
    label: label,
    labelPlacement: labelPlacement,
    isRequired: isRequired,
    styledComponent: styledComponents
  }), _react["default"].createElement("div", {
    className: type
  }, descriptionPlacement === 'above' && description && _react["default"].createElement("div", {
    className: "description"
  }, description), dateType && renderDateField(dateType), descriptionPlacement !== 'above' && description && _react["default"].createElement("div", {
    className: "description"
  }, description)));
};

exports["default"] = _default;