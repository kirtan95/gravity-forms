"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _ValidationMessage = _interopRequireDefault(require("../../FormElements/ValidationMessage"));

var _default = function _default(_ref) {
  var defaultProps = _ref.defaultProps;
  var field = defaultProps.field,
      validationMessage = defaultProps.validationMessage,
      touched = defaultProps.touched,
      setTouched = defaultProps.setTouched,
      updateForm = defaultProps.updateForm,
      formattedInputs = defaultProps.formattedInputs,
      error = defaultProps.error,
      unsetError = defaultProps.unsetError,
      setFocusClass = defaultProps.setFocusClass,
      styledComponents = defaultProps.styledComponents;
  var id = field.id,
      formId = field.formId,
      type = field.type,
      customName = field.customName;

  var _ref2 = styledComponents || false,
      _ref2$Input = _ref2.Input,
      Input = _ref2$Input === void 0 ? 'input' : _ref2$Input;

  return _react["default"].createElement(_react["default"].Fragment, null, formattedInputs && formattedInputs.map(function (item, index) {
    return _react["default"].createElement("div", {
      className: type,
      key: item.id
    }, _react["default"].createElement(Input, {
      id: "input_".concat(formId, "_").concat(id, "_").concat(index),
      type: "number",
      name: customName || "input_".concat(id, "[]"),
      placeholder: item.placeholder,
      step: "1",
      min: "1",
      max: item.label === 'MM' ? 12 : item.label === 'DD' ? 31 : new Date().getFullYear() + 1,
      maxLength: item.label === 'YYYY' ? 4 : 2,
      value: item.value,
      onBlur: function onBlur(event) {
        field.subId = index;
        field.dateLabel = item.label;
        updateForm(event, field);
        setTouched(id);
        unsetError(id);
        setFocusClass(item.value !== '');
      },
      onFocus: function onFocus() {
        return setFocusClass(true);
      }
    }), _react["default"].createElement("label", {
      htmlFor: "input_".concat(formId, "_").concat(id, "_").concat(index),
      className: "hide-label"
    }, item.label), validationMessage && touched && validationMessage[index] && index === validationMessage[index].index && validationMessage[index].message && _react["default"].createElement("span", {
      className: "error-message",
      id: "error_".concat(formId, "_").concat(item.id)
    }, validationMessage[index].message), error && _react["default"].createElement("span", {
      className: "error-message"
    }, error));
  }));
};

exports["default"] = _default;