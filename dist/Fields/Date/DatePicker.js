"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireDefault(require("react"));

var _reactDatepicker = _interopRequireDefault(require("react-datepicker"));

var _ValidationMessage = _interopRequireDefault(require("../../FormElements/ValidationMessage"));

require("react-datepicker/dist/react-datepicker.css");

var _default = function _default(_ref) {
  var format = _ref.format,
      defaultProps = _ref.defaultProps;
  var field = defaultProps.field,
      value = defaultProps.value,
      validationMessage = defaultProps.validationMessage,
      touched = defaultProps.touched,
      setTouched = defaultProps.setTouched,
      updateForm = defaultProps.updateForm,
      error = defaultProps.error,
      unsetError = defaultProps.unsetError,
      setFocusClass = defaultProps.setFocusClass,
      setDate = defaultProps.setDate,
      startDate = defaultProps.startDate,
      styledComponents = defaultProps.styledComponents,
      props = (0, _objectWithoutProperties2["default"])(defaultProps, ["field", "value", "validationMessage", "touched", "setTouched", "updateForm", "error", "unsetError", "setFocusClass", "setDate", "startDate", "styledComponents"]);
  var id = field.id,
      isRequired = field.isRequired,
      formId = field.formId,
      placeholder = field.placeholder,
      cssClass = field.cssClass,
      datepickerOptions = field.datepickerOptions,
      dateType = field.dateType;

  var _ref2 = styledComponents || false,
      _ref2$DatePicker = _ref2.DatePicker,
      SdatePicker = _ref2$DatePicker === void 0 ? "div" : _ref2$DatePicker;

  var adjustDatePickerOptions = function adjustDatePickerOptions(options) {
    if (dateType && dateType === 'datepicker' && options) {
      var keys = Object.keys(options);

      if (keys && keys.length > 0) {
        for (var i = 0; i < keys.length; i++) {
          if (keys[i] === 'minDate' || keys[i] === 'maxDate') {
            options[keys[i]] = new Date(options[keys[i]]);
          }
        }
      }
    }

    return options;
  };

  var dateOptions = adjustDatePickerOptions(datepickerOptions) || {};
  return _react["default"].createElement(_react["default"].Fragment, null, _react["default"].createElement(SdatePicker, {
    className: "ginput_container ginput_container_date"
  }, _react["default"].createElement(_reactDatepicker["default"], (0, _extends2["default"])({
    name: "input_".concat(id),
    id: "input_".concat(formId, "_").concat(id),
    type: "text",
    className: "datepicker hasDatepicker",
    selected: startDate,
    onChange: function onChange(date) {
      setDate(date);
      updateForm({
        target: {
          value: date
        }
      }, field);
      setTouched(id);
      unsetError(id);
      setFocusClass(date);
    },
    onBlur: function onBlur(e) {
      updateForm({
        target: {
          value: startDate
        }
      }, field);
      setTouched(id);
      unsetError(id);
      setFocusClass(startDate);
    },
    dateFormat: format || undefined,
    onFocus: function onFocus() {
      return setFocusClass(true);
    },
    autoComplete: "off",
    required: isRequired,
    placeholderText: placeholder,
    maxDate: cssClass.includes('past') && new Date()
  }, dateOptions))), (validationMessage && touched || error) && _react["default"].createElement(_ValidationMessage["default"], {
    validationMessage: validationMessage,
    error: error,
    id: id
  }));
};

exports["default"] = _default;