"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireDefault(require("react"));

var _reactSelect = _interopRequireDefault(require("react-select"));

var _ValidationMessage = _interopRequireDefault(require("../../FormElements/ValidationMessage"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var _default = function _default(_ref) {
  var defaultProps = _ref.defaultProps;
  var field = defaultProps.field,
      value = defaultProps.value,
      validationMessage = defaultProps.validationMessage,
      touched = defaultProps.touched,
      setTouched = defaultProps.setTouched,
      updateForm = defaultProps.updateForm,
      error = defaultProps.error,
      unsetError = defaultProps.unsetError,
      setFocusClass = defaultProps.setFocusClass,
      setDate = defaultProps.setDate,
      startDate = defaultProps.startDate,
      styledComponents = defaultProps.styledComponents,
      formattedInputs = defaultProps.formattedInputs,
      props = (0, _objectWithoutProperties2["default"])(defaultProps, ["field", "value", "validationMessage", "touched", "setTouched", "updateForm", "error", "unsetError", "setFocusClass", "setDate", "startDate", "styledComponents", "formattedInputs"]);
  var id = field.id,
      isRequired = field.isRequired,
      formId = field.formId,
      type = field.type,
      customName = field.customName;
  var RSelect = _reactSelect["default"] || 'select';

  var getNumberDropdown = function getNumberDropdown(i) {
    var options = [];
    var dayDropdownID = 0;
    var monthDropdownID = 1;
    var startNumber = i === dayDropdownID || i === monthDropdownID ? 1 : 1920;
    var endNumber = i === dayDropdownID ? 31 : i === monthDropdownID ? 12 : new Date().getFullYear();
    var selectedValue = i === dayDropdownID ? 1 : i === monthDropdownID ? 3 : 0;
    var increment = startNumber < endNumber ? 1 : -1;

    for (var _i = startNumber; _i !== endNumber + increment; _i += increment) {
      options.push({
        value: _i,
        label: _i,
        selected: parseInt(_i) === parseInt(selectedValue)
      });
    }

    return options;
  };

  var handleChange = function handleChange(value, field, index) {
    var tmp = startDate || [];
    var key = index === 0 ? 2 : index === 2 ? 0 : 1;
    tmp[key] = value.value;
    setDate(tmp);
    var event = {
      target: {
        value: value.value
      }
    };
    updateForm(event, field);
  };

  var getValueByIndex = function getValueByIndex(index) {
    if (!startDate) return;
    var i = index === 0 ? 2 : index === 1 ? 1 : 0;
    return startDate[i] ? {
      value: startDate[i],
      label: startDate[i]
    } : undefined;
  };

  return _react["default"].createElement(_react["default"].Fragment, null, formattedInputs && formattedInputs.map(function (input, index) {
    return _react["default"].createElement("div", {
      key: input.id,
      className: "gfield_date_dropdown"
    }, _react["default"].createElement(RSelect, {
      required: isRequired,
      placeholder: input.placeholder,
      options: getNumberDropdown(index),
      value: getValueByIndex(index),
      id: "input_".concat(formId, "_").concat(id, "_").concat(index + 1),
      name: customName || "input_".concat(id, "[]"),
      onBlur: function onBlur(event) {
        var value = getValueByIndex(index);

        if (!value) {
          var tmpState = _objectSpread(_objectSpread({}, field), {}, {
            subId: index,
            dateLabel: input.label
          });

          handleChange({
            value: ''
          }, tmpState, index);
        }

        setTouched(id);
        unsetError(id);
        setFocusClass(input.value !== '');
      },
      onChange: function onChange(option) {
        var tmpState = _objectSpread(_objectSpread({}, field), {}, {
          subId: index,
          dateLabel: input.label
        });

        handleChange(option, tmpState, index);
        unsetError(id);
      },
      onFocus: function onFocus() {
        return setFocusClass(true);
      }
    }));
  }), (validationMessage && touched || error) && _react["default"].createElement(_ValidationMessage["default"], {
    validationMessage: validationMessage,
    error: error,
    id: id
  }));
};

exports["default"] = _default;