"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireWildcard(require("react"));

var _InputSubLabel = _interopRequireDefault(require("../FormElements/InputSubLabel"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var _default = function _default(_ref) {
  var field = _ref.field,
      value = _ref.value,
      validationMessage = _ref.validationMessage,
      touched = _ref.touched,
      setTouched = _ref.setTouched,
      hideField = _ref.hideField,
      updateForm = _ref.updateForm,
      styledComponents = _ref.styledComponents,
      error = _ref.error,
      unsetError = _ref.unsetError,
      cssClass = _ref.cssClass,
      setFocusClass = _ref.setFocusClass,
      props = (0, _objectWithoutProperties2["default"])(_ref, ["field", "value", "validationMessage", "touched", "setTouched", "hideField", "updateForm", "styledComponents", "error", "unsetError", "cssClass", "setFocusClass"]);
  var id = field.id,
      formId = field.formId,
      type = field.type,
      label = field.label,
      placeholder = field.placeholder,
      isRequired = field.isRequired,
      maxLength = field.maxLength,
      description = field.description,
      descriptionPlacement = field.descriptionPlacement,
      labelPlacement = field.labelPlacement,
      width = field.width,
      customName = field.customName,
      emailConfirmEnabled = field.emailConfirmEnabled,
      inputs = field.inputs;

  var _useState = (0, _react.useState)(inputs),
      _useState2 = (0, _slicedToArray2["default"])(_useState, 2),
      emails = _useState2[0],
      setEmails = _useState2[1];

  var setFocusClassMultiple = function setFocusClassMultiple(action, i) {
    var email = _objectSpread({}, emails);

    if (!email[i]) return;

    if (action && email && !!email.length > 0) {
      email[i].cssClass = "filled";
    } else {
      email[i].cssClass = "";
    }

    setEmails(email);
  };

  var _ref2 = styledComponents || false,
      _ref2$Input = _ref2.Input,
      Input = _ref2$Input === void 0 ? "input" : _ref2$Input,
      _ref2$Label = _ref2.Label,
      Label = _ref2$Label === void 0 ? "label" : _ref2$Label,
      _ref2$Box = _ref2.Box,
      Box = _ref2$Box === void 0 ? "div" : _ref2$Box,
      _ref2$FieldSet = _ref2.FieldSet,
      FieldSet = _ref2$FieldSet === void 0 ? 'fieldset' : _ref2$FieldSet,
      _ref2$Legend = _ref2.Legend,
      Legend = _ref2$Legend === void 0 ? 'legend' : _ref2$Legend;

  return _react["default"].createElement(FieldSet, {
    width: width,
    className: validationMessage && touched || error ? "form-field error ".concat(cssClass) : "form-field ".concat(cssClass),
    style: {
      display: hideField ? "none" : undefined
    }
  }, _react["default"].createElement(Legend, {
    className: "field-label"
  }, label, isRequired ? _react["default"].createElement("abbr", null, "*") : null), descriptionPlacement === "above" && description && _react["default"].createElement("div", {
    className: "description"
  }, description), _react["default"].createElement("div", {
    className: "input-complex"
  }, emailConfirmEnabled ? _react["default"].createElement(_react["default"].Fragment, null, inputs && inputs.length && inputs.map(function (input, i) {
    return _react["default"].createElement("span", {
      key: "input_".concat(formId, "_").concat(input.id),
      className: "".concat(inputs.length > 1 ? "ginput_".concat(i === 0 ? "left" : "right") : "medim", " ").concat(emails[i].cssClass ? emails[i].cssClass : "")
    }, labelPlacement === 'above' && _react["default"].createElement(_InputSubLabel["default"], {
      formId: formId,
      id: input.id,
      label: input.label,
      labelPlacement: labelPlacement,
      styledComponent: styledComponents
    }), _react["default"].createElement(Input, {
      id: "input_".concat(formId, "_").concat(input.id),
      name: customName || "input_".concat(id).concat(i === 1 ? "_".concat(i + 1) : ""),
      type: type,
      value: value && value[i] && value[i].val ? value[i].val : "",
      placeholder: input.placeholder ? input.placeholder : placeholder,
      required: isRequired,
      autoComplete: "off",
      onChange: function onChange(event) {
        field.subId = i;
        updateForm(event, field);
        unsetError(id);
      },
      onBlur: function onBlur(event) {
        field.subId = i;
        updateForm(event, field);
        setTouched(id);
        setFocusClassMultiple(value && value[i] && value[i].val && value[i].val !== "", i);
      },
      onFocus: function onFocus() {
        return setFocusClassMultiple(true, i);
      },
      "aria-label": label,
      "aria-describedby": "error_".concat(formId, "_").concat(input.id, "_").concat(i),
      "aria-invalid": !!validationMessage && touched
    }), labelPlacement !== 'above' && _react["default"].createElement(_InputSubLabel["default"], {
      formId: formId,
      id: input.id,
      label: input.label,
      labelPlacement: labelPlacement,
      styledComponent: styledComponents
    }));
  })) : _react["default"].createElement(_react["default"].Fragment, null, _react["default"].createElement(Input, {
    id: "input_".concat(formId, "_").concat(id),
    name: customName || "input_".concat(id),
    type: type,
    value: !value ? "" : value,
    placeholder: placeholder,
    maxLength: maxLength,
    required: isRequired,
    onChange: function onChange(event) {
      updateForm(event, field);
      unsetError(id);
    },
    onBlur: function onBlur(event) {
      updateForm(event, field);
      setTouched(id);
      setFocusClass(value !== "");
    },
    onFocus: function onFocus() {
      return setFocusClass(true);
    },
    "aria-label": label,
    "aria-describedby": "error_".concat(formId, "_").concat(id),
    "aria-invalid": !!validationMessage && touched || !!error
  }))), descriptionPlacement !== "above" && description && _react["default"].createElement("div", {
    className: "description"
  }, description), (validationMessage && touched || error) && _react["default"].createElement("span", {
    className: "error-message",
    id: "error_".concat(formId, "_").concat(id)
  }, validationMessage || error));
};

exports["default"] = _default;