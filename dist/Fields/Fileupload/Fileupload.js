"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireWildcard(require("react"));

var _reactDropzone = _interopRequireDefault(require("react-dropzone"));

var _GFDropzone = _interopRequireDefault(require("./GFDropzone"));

var _InputLabel = _interopRequireDefault(require("../../FormElements/InputLabel"));

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

var Fileupload = function (_Component) {
  (0, _inherits2["default"])(Fileupload, _Component);

  var _super = _createSuper(Fileupload);

  function Fileupload() {
    var _this;

    (0, _classCallCheck2["default"])(this, Fileupload);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "state", {
      imagePreviewUrl: _this.props.field.preview || null,
      selectedFile: _this.props.field.preview ? true : null,
      uploadFileText: "No file chosen",
      previewID: _this.props.value || null,
      errorText: _this.props.error || false
    });
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "inputFile", _react["default"].createRef());
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "onChangeHandler", function (event) {
      var _this$props$field = _this.props.field,
          hasPreview = _this$props$field.hasPreview,
          allowedExtensions = _this$props$field.allowedExtensions;

      _this.setState({
        imagePreviewUrl: null,
        selectedFile: event.target.files[0],
        uploadFileText: event.target.files[0] ? event.target.files[0].name : "No file chosen"
      });

      if (hasPreview && event.target && event.target.files[0]) {
        var extension = event.target.files[0].name.split(".").pop().toLowerCase();
        var isSuccess = allowedExtensions.indexOf(extension) > -1;

        if (isSuccess) {
          var reader = new FileReader();

          reader.onloadend = function () {
            _this.setState({
              imagePreviewUrl: reader.result,
              previewID: false
            });
          };

          reader.readAsDataURL(event.target.files[0]);
        }
      }
    });
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "removeFilePreview", function () {
      var _this$props = _this.props,
          field = _this$props.field,
          unsetError = _this$props.unsetError;
      _this.inputFile.current.value = "";

      _this.setState({
        imagePreviewUrl: null,
        selectedFile: null,
        previewID: false
      });

      unsetError(field.id);
    });
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "prepareAllowedTypes", function (types) {
      var accept = types.split(",");
      accept = accept.map(function (str) {
        return ".".concat(str.replace(/\s/g, ""));
      }).join(", ");
      return accept;
    });
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "onButtonClickHandler", function () {
      _this.inputFile.current.click();
    });
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "removeFile", function (e, field) {
      e.preventDefault();
      var updateForm = _this.props.updateForm;
      updateForm({
        target: {
          value: ""
        }
      }, field);
      _this.inputFile.current.value = "";

      _this.setState({
        imagePreviewUrl: false,
        selectedFile: false,
        previewID: false,
        uploadFileText: "No file chosen"
      });
    });
    return _this;
  }

  (0, _createClass2["default"])(Fileupload, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state = this.state,
          selectedFile = _this$state.selectedFile,
          uploadFileText = _this$state.uploadFileText,
          imagePreviewUrl = _this$state.imagePreviewUrl,
          previewID = _this$state.previewID;
      var _this$props2 = this.props,
          field = _this$props2.field,
          value = _this$props2.value,
          validationMessage = _this$props2.validationMessage,
          touched = _this$props2.touched,
          setTouched = _this$props2.setTouched,
          hideField = _this$props2.hideField,
          updateForm = _this$props2.updateForm,
          formID = _this$props2.formID,
          fieldError = _this$props2.fieldError,
          styledComponents = _this$props2.styledComponents,
          error = _this$props2.error,
          unsetError = _this$props2.unsetError,
          dropzoneText = _this$props2.dropzoneText;
      var id = field.id,
          type = field.type,
          label = field.label,
          cssClass = field.cssClass,
          isRequired = field.isRequired,
          description = field.description,
          descriptionPlacement = field.descriptionPlacement,
          labelPlacement = field.labelPlacement,
          width = field.width,
          allowedExtensions = field.allowedExtensions,
          buttonText = field.buttonText,
          hasPreview = field.hasPreview,
          maxFileSize = field.maxFileSize;

      var _ref = styledComponents || false,
          _ref$Button = _ref.Button,
          Button = _ref$Button === void 0 ? "button" : _ref$Button,
          _ref$Label = _ref.Label,
          Label = _ref$Label === void 0 ? "label" : _ref$Label,
          _ref$FileWrapper = _ref.FileWrapper,
          FileWrapper = _ref$FileWrapper === void 0 ? "div" : _ref$FileWrapper,
          _ref$Box = _ref.Box,
          Box = _ref$Box === void 0 ? "div" : _ref$Box;

      var hasDropzone = cssClass.indexOf("dropzone") > -1;
      return _react["default"].createElement(Box, {
        width: width,
        className: validationMessage && touched || error ? "form-field error ".concat(cssClass) : "form-field ".concat(cssClass),
        style: {
          display: hideField ? "none" : undefined
        }
      }, _react["default"].createElement(FileWrapper, {
        className: type
      }, _react["default"].createElement(_InputLabel["default"], {
        formId: formID,
        id: id,
        label: label,
        labelPlacement: labelPlacement,
        isRequired: isRequired,
        styledComponent: styledComponents
      }), descriptionPlacement === "above" && description && description && _react["default"].createElement("div", {
        className: "description"
      }, description), hasDropzone ? _react["default"].createElement(_GFDropzone["default"], {
        dropzoneText: dropzoneText,
        field: field,
        id: id,
        formID: formID,
        isRequired: isRequired,
        updateForm: updateForm,
        setTouched: setTouched,
        unsetError: unsetError
      }) : _react["default"].createElement(_react["default"].Fragment, null, maxFileSize && _react["default"].createElement("input", {
        type: "hidden",
        name: "MAX_FILE_SIZE",
        value: maxFileSize * 1048576
      }), _react["default"].createElement("input", {
        id: "input_".concat(formID, "_").concat(id),
        name: "input_".concat(id),
        type: "file",
        required: isRequired,
        ref: this.inputFile,
        onChange: function onChange(event) {
          var _event$target, _event$target$files, _event$target$files$;

          _this2.onChangeHandler(event);

          updateForm({
            target: {
              value: event === null || event === void 0 ? void 0 : (_event$target = event.target) === null || _event$target === void 0 ? void 0 : (_event$target$files = _event$target.files) === null || _event$target$files === void 0 ? void 0 : (_event$target$files$ = _event$target$files[0]) === null || _event$target$files$ === void 0 ? void 0 : _event$target$files$.name
            }
          }, field);
          setTouched(id);
          unsetError(id);
        },
        onBlur: function onBlur(event) {
          var _event$target2, _event$target2$files, _event$target2$files$;

          updateForm({
            target: {
              value: event === null || event === void 0 ? void 0 : (_event$target2 = event.target) === null || _event$target2 === void 0 ? void 0 : (_event$target2$files = _event$target2.files) === null || _event$target2$files === void 0 ? void 0 : (_event$target2$files$ = _event$target2$files[0]) === null || _event$target2$files$ === void 0 ? void 0 : _event$target2$files$.name
            }
          }, field);
          setTouched(id);
        },
        accept: this.prepareAllowedTypes(allowedExtensions) || undefined,
        "aria-label": label,
        "aria-describedby": "error_".concat(formID, "_").concat(id),
        "aria-invalid": !!validationMessage || !!error,
        hidden: "hidden"
      }), previewID && field.preview && _react["default"].createElement("input", {
        type: "hidden",
        name: "file-upload-preview",
        value: previewID
      }), hasPreview && _react["default"].createElement("div", {
        className: "file-preview",
        style: selectedFile && imagePreviewUrl ? {
          backgroundImage: "url(".concat(imagePreviewUrl, ")")
        } : undefined
      }, selectedFile && imagePreviewUrl && _react["default"].createElement("button", {
        type: "button",
        className: "remove-file",
        onClick: function onClick() {
          return _this2.removeFilePreview();
        }
      })), _react["default"].createElement("div", {
        "aria-pressed": "false",
        tabIndex: "0",
        role: "button",
        className: "fileUpload",
        onClick: this.onButtonClickHandler
      }, _react["default"].createElement(Button, {
        color: "yellow",
        tabIndex: "-1",
        type: "button"
      }, buttonText || "Choose a file"), !selectedFile && _react["default"].createElement("span", {
        className: "no-file"
      }, uploadFileText)), selectedFile && _react["default"].createElement("div", null, _react["default"].createElement("button", {
        type: "button",
        onClick: function onClick(e) {
          return _this2.removeFile(e, field);
        }
      }, "remove file"), _react["default"].createElement("span", null, uploadFileText)), description && descriptionPlacement !== "above" && _react["default"].createElement("div", {
        className: "description"
      }, description)), (validationMessage && touched || error) && _react["default"].createElement("span", {
        className: "error-message",
        id: "error_".concat(id)
      }, validationMessage || error), fieldError && _react["default"].createElement("span", {
        className: "error-message",
        id: "error_".concat(formID, "_").concat(id)
      }, fieldError)));
    }
  }]);
  return Fileupload;
}(_react.Component);

var _default = Fileupload;
exports["default"] = _default;