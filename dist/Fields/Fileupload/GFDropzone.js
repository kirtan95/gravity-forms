"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireWildcard(require("react"));

var _reactDropzone = require("react-dropzone");

function Accept(_ref) {
  var dropzoneText = _ref.dropzoneText,
      props = (0, _objectWithoutProperties2["default"])(_ref, ["dropzoneText"]);
  var field = props.field;

  var _ref2 = field || [],
      defaultValue = _ref2.defaultValue;

  var _useState = (0, _react.useState)(defaultValue ? [defaultValue] : []),
      _useState2 = (0, _slicedToArray2["default"])(_useState, 2),
      files = _useState2[0],
      setFiles = _useState2[1];

  var _useState3 = (0, _react.useState)('dropzone'),
      _useState4 = (0, _slicedToArray2["default"])(_useState3, 2),
      cssClass = _useState4[0],
      setcssClass = _useState4[1];

  var _useDropzone = (0, _reactDropzone.useDropzone)({
    accept: 'image/*',
    onDrop: function onDrop(event) {
      var id = props.id,
          formID = props.formID,
          field = props.field,
          isRequired = props.isRequired,
          updateForm = props.updateForm,
          setTouched = props.setTouched,
          unsetError = props.unsetError;
      setFiles(event.map(function (file) {
        return Object.assign(file, {
          preview: URL.createObjectURL(file)
        });
      }));
      updateForm(event, field);
      setTouched(id);
      unsetError(id);
    },
    onDragOver: function onDragOver(event) {
      setcssClass('dropzone over');
    },
    onDragLeave: function onDragLeave(event) {
      setcssClass('dropzone');
    },
    onDropAccepted: function onDropAccepted(event) {
      setcssClass('dropzone');
    }
  }),
      getRootProps = _useDropzone.getRootProps,
      getInputProps = _useDropzone.getInputProps,
      isDragActive = _useDropzone.isDragActive,
      isDragAccept = _useDropzone.isDragAccept,
      isDragReject = _useDropzone.isDragReject;

  var thumbs = files && !!files.length > 0 && files.map(function (file) {
    return _react["default"].createElement("div", {
      key: file.name
    }, _react["default"].createElement("div", null, _react["default"].createElement("img", {
      src: file.preview
    })));
  });
  var id = props.id,
      formID = props.formID,
      isRequired = props.isRequired;
  (0, _react.useEffect)(function () {
    return function () {
      files.forEach(function (file) {
        return URL.revokeObjectURL(file.preview);
      });
    };
  }, [files]);
  var text = dropzoneText || 'Drag \'n\' drop some files here, or click to select files';
  return _react["default"].createElement("div", {
    className: "container"
  }, _react["default"].createElement("div", getRootProps({
    className: cssClass
  }), _react["default"].createElement("input", (0, _extends2["default"])({
    id: "input_".concat(formID, "_").concat(id),
    name: "input_".concat(id),
    type: "file",
    required: isRequired
  }, getInputProps())), thumbs && _react["default"].createElement("div", {
    className: "preview"
  }, thumbs), _react["default"].createElement("p", {
    dangerouslySetInnerHTML: {
      __html: text
    }
  })));
}

var _default = Accept;
exports["default"] = _default;