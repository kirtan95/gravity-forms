"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireDefault(require("react"));

var _queryString = _interopRequireDefault(require("query-string"));

var _default = function _default(_ref) {
  var field = _ref.field,
      value = _ref.value,
      validationMessage = _ref.validationMessage,
      touched = _ref.touched,
      setTouched = _ref.setTouched,
      updateForm = _ref.updateForm,
      cssClass = _ref.cssClass,
      props = (0, _objectWithoutProperties2["default"])(_ref, ["field", "value", "validationMessage", "touched", "setTouched", "updateForm", "cssClass"]);
  var id = field.id,
      type = field.type,
      isRequired = field.isRequired,
      customName = field.customName,
      formId = field.formId;
  var prePopulated = false;

  if (field.allowsPrepopulate) {
    var queries = _queryString["default"].parse(location.search);

    prePopulated = queries[field.inputName];
  }

  return _react["default"].createElement("div", {
    className: cssClass
  }, _react["default"].createElement("input", {
    name: customName || "input_".concat(id),
    type: type,
    value: !prePopulated ? value : prePopulated,
    required: isRequired,
    onChange: function onChange(event) {
      return updateForm(event, field);
    },
    onBlur: function onBlur(event) {
      updateForm(event, field);
      setTouched(id);
    },
    "aria-describedby": "error_".concat(formId, "_").concat(id),
    "aria-invalid": !!validationMessage
  }), validationMessage && touched && _react["default"].createElement("span", {
    className: "error-message",
    id: "error_".concat(formId, "_").concat(id)
  }, validationMessage));
};

exports["default"] = _default;