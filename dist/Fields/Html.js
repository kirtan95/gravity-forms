"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _default = function _default(_ref) {
  var field = _ref.field,
      saveStateToHtmlField = _ref.saveStateToHtmlField,
      hideField = _ref.hideField,
      styledComponents = _ref.styledComponents;
  var content = field.content,
      label = field.label,
      cssClass = field.cssClass,
      width = field.width;

  if (saveStateToHtmlField) {
    var states = saveStateToHtmlField.saveStateToHtmlField,
        formValues = saveStateToHtmlField.formValues;
    var classes = Object.keys(states);
    var values = [];

    if (classes) {
      for (var id in formValues) {
        var _field = formValues[id];
        var value = "";

        for (var y = 0; y < classes.length; y++) {
          if (_field.cssClass.indexOf(classes[y]) !== -1 && !_field.valid) {
            values[states[classes[y]]] = _field.value;
          }
        }
      }
    }

    if (values) {
      var replacedValues = Object.keys(values);

      for (var i = 0; i < replacedValues.length; i++) {
        if (replacedValues[i] === "%price%") {
          values[replacedValues[i]] = "\u20AC".concat(values[replacedValues[i]]);
        }

        content = content.replace(replacedValues[i], values[replacedValues[i]]);
      }
    }
  }

  var _ref2 = styledComponents || false,
      _ref2$Box = _ref2.Box,
      Box = _ref2$Box === void 0 ? "div" : _ref2$Box,
      _ref2$Label = _ref2.Label,
      Label = _ref2$Label === void 0 ? "label" : _ref2$Label;

  return _react["default"].createElement(Box, {
    width: width,
    className: "form-field ".concat(cssClass),
    style: {
      display: hideField ? "none" : undefined
    }
  }, _react["default"].createElement(Label, {
    className: "gf-label"
  }, label), _react["default"].createElement("div", {
    className: "html-content",
    dangerouslySetInnerHTML: {
      __html: content
    }
  }));
};

exports["default"] = _default;