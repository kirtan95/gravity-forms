"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireWildcard(require("react"));

var _reactSelect = _interopRequireDefault(require("react-select"));

var _InputLabel = _interopRequireDefault(require("../FormElements/InputLabel"));

var _InputSubLabel = _interopRequireDefault(require("../FormElements/InputSubLabel"));

var _default = function _default(_ref) {
  var _prefixField$choices;

  var field = _ref.field,
      value = _ref.value,
      validationMessage = _ref.validationMessage,
      touched = _ref.touched,
      setTouched = _ref.setTouched,
      hideField = _ref.hideField,
      updateForm = _ref.updateForm,
      styledComponents = _ref.styledComponents,
      error = _ref.error,
      unsetError = _ref.unsetError,
      setFocusClass = _ref.setFocusClass,
      cssClass = _ref.cssClass,
      props = (0, _objectWithoutProperties2["default"])(_ref, ["field", "value", "validationMessage", "touched", "setTouched", "hideField", "updateForm", "styledComponents", "error", "unsetError", "setFocusClass", "cssClass"]);
  var id = field.id,
      formId = field.formId,
      type = field.type,
      label = field.label,
      placeholder = field.placeholder,
      isRequired = field.isRequired,
      inputs = field.inputs,
      maxLength = field.maxLength,
      description = field.description,
      descriptionPlacement = field.descriptionPlacement,
      labelPlacement = field.labelPlacement,
      width = field.width,
      customName = field.customName;

  var _ref2 = styledComponents || false,
      ReactSelect = _ref2.ReactSelect,
      _ref2$Input = _ref2.Input,
      Input = _ref2$Input === void 0 ? 'input' : _ref2$Input,
      _ref2$Label = _ref2.Label,
      Label = _ref2$Label === void 0 ? 'label' : _ref2$Label,
      _ref2$Box = _ref2.Box,
      Box = _ref2$Box === void 0 ? 'div' : _ref2$Box,
      _ref2$FieldSet = _ref2.FieldSet,
      FieldSet = _ref2$FieldSet === void 0 ? 'fieldset' : _ref2$FieldSet,
      _ref2$Legend = _ref2.Legend,
      Legend = _ref2$Legend === void 0 ? 'legend' : _ref2$Legend;

  var RSelect = ReactSelect || _reactSelect["default"];
  var prefixField = inputs[0];
  var options = prefixField === null || prefixField === void 0 ? void 0 : (_prefixField$choices = prefixField.choices) === null || _prefixField$choices === void 0 ? void 0 : _prefixField$choices.map(function (choice) {
    return {
      value: choice.value,
      label: choice.text
    };
  });
  var preselected = prefixField === null || prefixField === void 0 ? void 0 : prefixField.choices.filter(function (choice) {
    return choice.isSelected;
  });

  if (preselected.length) {
    preselected = [{
      label: preselected[0].text,
      value: preselected[0].value
    }];
  } else {
    preselected = '';
  }

  var _useState = (0, _react.useState)(value || preselected),
      _useState2 = (0, _slicedToArray2["default"])(_useState, 2),
      selectedOption = _useState2[0],
      selectOption = _useState2[1];

  var selectChange = function selectChange(option) {
    selectOption(option);
  };

  return _react["default"].createElement(FieldSet, {
    width: width,
    className: validationMessage && touched || error ? "form-field error ".concat(cssClass) : "form-field ".concat(cssClass),
    style: {
      display: hideField ? 'none' : undefined
    }
  }, _react["default"].createElement(Legend, {
    className: "field-label"
  }, label, isRequired ? _react["default"].createElement("abbr", null, "*") : null), descriptionPlacement === 'above' && description && _react["default"].createElement("div", {
    className: "description"
  }, description), _react["default"].createElement("div", {
    className: "input-complex"
  }, inputs.map(function (input, index) {
    return !input.isHidden && _react["default"].createElement("div", {
      className: type,
      key: input.id
    }, labelPlacement === 'above' && _react["default"].createElement(_InputSubLabel["default"], {
      formId: formId,
      id: input.id,
      label: input.label,
      labelPlacement: labelPlacement,
      styledComponent: styledComponents
    }), input.inputType === 'radio' ? _react["default"].createElement(RSelect, {
      name: customName || "input_".concat(input.id),
      required: isRequired,
      value: selectedOption && selectedOption.value ? selectedOption : '',
      onChange: function onChange(option) {
        selectChange(option, field);
        updateForm(option, field, '-1');
        unsetError(input.id);
      },
      onFocus: function onFocus() {
        return setFocusClass(true);
      },
      placeholder: input.placeholder,
      options: options,
      className: "form-select",
      autoFocus: false,
      inputId: "input_".concat(formId, "_").concat(input.id),
      "data-key": input.key
    }) : _react["default"].createElement(Input, {
      id: "input_".concat(formId, "_").concat(input.id),
      key: input.id,
      name: customName || "input_".concat(input.id),
      type: type,
      value: !value ? '' : value[input.id],
      placeholder: input.placeholder,
      maxLength: maxLength,
      required: isRequired,
      onChange: function onChange(event) {
        updateForm(event, field, input.id);
        unsetError(input.id);
      },
      onBlur: function onBlur(event) {
        updateForm(event, field);
        setTouched(input.id);
        setFocusClass(value !== '');
      },
      onFocus: function onFocus() {
        return setFocusClass(true);
      },
      "data-key": input.key,
      "aria-label": input.label,
      "aria-describedby": "error_".concat(formId, "_").concat(input.id),
      "aria-invalid": !!validationMessage && touched || !!error
    }), labelPlacement !== 'above' && _react["default"].createElement(_InputSubLabel["default"], {
      formId: formId,
      id: input.id,
      label: input.label,
      labelPlacement: labelPlacement,
      styledComponent: styledComponents
    }));
  })), _react["default"].createElement("div", null, descriptionPlacement !== 'above' && description && _react["default"].createElement("div", {
    className: "description"
  }, description)), (validationMessage && touched || error) && _react["default"].createElement("span", {
    className: "error-message",
    id: "error_".concat(formId, "_").concat(id)
  }, validationMessage || error));
};

exports["default"] = _default;