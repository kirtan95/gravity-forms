"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _default = function _default(_ref) {
  var field = _ref.field,
      pages = _ref.pages,
      nextStep = _ref.nextStep,
      prevStep = _ref.prevStep,
      isNextDisabled = _ref.isNextDisabled,
      styledComponents = _ref.styledComponents;
  var id = field.id,
      type = field.type,
      label = field.label,
      placeholder = field.placeholder,
      isRequired = field.isRequired,
      maxLength = field.maxLength,
      description = field.description,
      descriptionPlacement = field.descriptionPlacement,
      labelPlacement = field.labelPlacement,
      cssClass = field.cssClass,
      width = field.width,
      nextButton = field.nextButton,
      previousButton = field.previousButton,
      pageNumber = field.pageNumber;

  var _ref2 = styledComponents || false,
      _ref2$Box = _ref2.Box,
      Box = _ref2$Box === void 0 ? 'div' : _ref2$Box,
      _ref2$Button = _ref2.Button,
      Button = _ref2$Button === void 0 ? 'button' : _ref2$Button;

  return _react["default"].createElement(Box, {
    className: "form-field"
  }, pageNumber - 1 > 1 && _react["default"].createElement(Button, {
    className: "prev",
    mr: 10,
    onClick: function onClick(e) {
      return prevStep(e);
    }
  }, previousButton.text), pageNumber <= pages && _react["default"].createElement(Button, {
    className: "next",
    onClick: function onClick(e) {
      return nextStep(e);
    },
    mt: 20,
    disabled: isNextDisabled
  }, nextButton.text));
};

exports["default"] = _default;