"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireWildcard(require("react"));

var _InputLabel = _interopRequireDefault(require("../FormElements/InputLabel"));

var _InputSubLabel = _interopRequireDefault(require("../FormElements/InputSubLabel"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var _default = function _default(_ref) {
  var field = _ref.field,
      value = _ref.value,
      validationMessage = _ref.validationMessage,
      touched = _ref.touched,
      setTouched = _ref.setTouched,
      hideField = _ref.hideField,
      updateForm = _ref.updateForm,
      styledComponents = _ref.styledComponents,
      error = _ref.error,
      unsetError = _ref.unsetError,
      props = (0, _objectWithoutProperties2["default"])(_ref, ["field", "value", "validationMessage", "touched", "setTouched", "hideField", "updateForm", "styledComponents", "error", "unsetError"]);
  var id = field.id,
      formId = field.formId,
      type = field.type,
      label = field.label,
      placeholder = field.placeholder,
      isRequired = field.isRequired,
      maxLength = field.maxLength,
      description = field.description,
      descriptionPlacement = field.descriptionPlacement,
      labelPlacement = field.labelPlacement,
      width = field.width,
      customName = field.customName,
      inputs = field.inputs,
      cssClass = field.cssClass,
      passwordStrengthEnabled = field.passwordStrengthEnabled,
      minPasswordStrength = field.minPasswordStrength;

  var _ref2 = styledComponents || false,
      _ref2$Input = _ref2.Input,
      Input = _ref2$Input === void 0 ? "input" : _ref2$Input,
      _ref2$Label = _ref2.Label,
      Label = _ref2$Label === void 0 ? "label" : _ref2$Label,
      _ref2$Box = _ref2.Box,
      Box = _ref2$Box === void 0 ? "div" : _ref2$Box,
      _ref2$Fieldset = _ref2.Fieldset,
      Fieldset = _ref2$Fieldset === void 0 ? 'fieldset' : _ref2$Fieldset,
      _ref2$Legend = _ref2.Legend,
      Legend = _ref2$Legend === void 0 ? 'legend' : _ref2$Legend;

  var _useState = (0, _react.useState)(inputs),
      _useState2 = (0, _slicedToArray2["default"])(_useState, 2),
      passwords = _useState2[0],
      setPasswords = _useState2[1];

  var _useState3 = (0, _react.useState)("blank"),
      _useState4 = (0, _slicedToArray2["default"])(_useState3, 2),
      passwordStrength = _useState4[0],
      setPasswordStrength = _useState4[1];

  var setFocusClass = function setFocusClass(action, i) {
    var pass = _objectSpread({}, passwords);

    if (action) {
      pass[i].cssClass = "filled";
    } else {
      pass[i].cssClass = "";
    }

    setPasswords(pass);
  };

  var gformPasswordStrength = function gformPasswordStrength(password1) {
    var shortPass = 1;
    var badPass = 2;
    var goodPass = 3;
    var strongPass = 4;
    var mismatch = 5;
    var symbolSize = 0;
    var natLog;
    var score;
    if (password1.length <= 0) return "blank";
    if (password1.length < 4) return "short";
    if (password1.match(/[0-9]/)) symbolSize += 10;
    if (password1.match(/[a-z]/)) symbolSize += 26;
    if (password1.match(/[A-Z]/)) symbolSize += 26;
    if (password1.match(/[^a-zA-Z0-9]/)) symbolSize += 31;
    natLog = Math.log(Math.pow(symbolSize, password1.length));
    score = natLog / Math.LN2;
    if (score < 40) return "bad";
    if (score < 56) return "good";
    return "strong";
  };

  var setGFPwdStrength = function setGFPwdStrength(password) {
    var result = gformPasswordStrength(password);
    setPasswordStrength(result);
  };

  return _react["default"].createElement(Fieldset, {
    width: width,
    className: validationMessage && touched || error ? "form-field error ".concat(cssClass) : "form-field ".concat(cssClass),
    style: {
      display: hideField ? "none" : undefined
    }
  }, _react["default"].createElement(Legend, {
    className: "field-label"
  }, label, isRequired ? _react["default"].createElement("abbr", null, "*") : null), descriptionPlacement === 'above' && description && _react["default"].createElement("div", {
    className: "description"
  }, description), _react["default"].createElement("div", {
    className: "input-complex"
  }, inputs && inputs.length && inputs.map(function (input, i) {
    return !input.isHidden && _react["default"].createElement("span", {
      key: "input_".concat(formId, "_").concat(input.id),
      className: "".concat(inputs.length > 1 ? "ginput_".concat(i === 0 ? "left" : "right") : "medim", " ").concat(passwords[i].cssClass ? passwords[i].cssClass : "")
    }, labelPlacement === 'above' && _react["default"].createElement(_InputSubLabel["default"], {
      formId: formId,
      id: input.id,
      label: input.label,
      labelPlacement: labelPlacement,
      styledComponent: styledComponents
    }), _react["default"].createElement(Input, {
      id: "input_".concat(formId, "_").concat(input.id),
      name: customName || "input_".concat(id).concat(i === 1 ? "_".concat(i + 1) : ""),
      type: type,
      value: value && value[i] && value[i].val ? value[i].val : "",
      placeholder: input.placeholder ? input.placeholder : placeholder,
      required: isRequired,
      autoComplete: "off",
      onChange: function onChange(event) {
        field.subId = i;
        updateForm(event, field);
        unsetError(id);

        if (passwordStrengthEnabled && i === 0) {
          setGFPwdStrength(event.target.value);
        }
      },
      onBlur: function onBlur(event) {
        field.subId = i;
        updateForm(event, field);
        setTouched(id);
        setFocusClass(value && value[i] && value[i].val && value[i].val !== "", i);
      },
      onFocus: function onFocus() {
        return setFocusClass(true, i);
      },
      "aria-label": label,
      "aria-describedby": "error_".concat(formId, "_").concat(input.id, "_").concat(i),
      "aria-invalid": !!validationMessage && touched
    }), labelPlacement !== 'above' && _react["default"].createElement(_InputSubLabel["default"], {
      formId: formId,
      id: input.id,
      label: input.label,
      labelPlacement: labelPlacement,
      styledComponent: styledComponents
    }));
  })), (validationMessage && touched || error) && _react["default"].createElement("span", {
    className: "error-message",
    id: "error_".concat(formId, "_").concat(id),
    dangerouslySetInnerHTML: {
      __html: validationMessage || error
    }
  }), descriptionPlacement !== "above" && description && _react["default"].createElement("div", {
    className: "description",
    dangerouslySetInnerHTML: {
      __html: description.replace(/\n/g, "<br />")
    }
  }), !(validationMessage && touched || error) && passwordStrengthEnabled && _react["default"].createElement(_react["default"].Fragment, null, _react["default"].createElement("div", {
    id: "input_".concat(formId, "_").concat(id, "_strength_indicator"),
    className: "gfield_password_strength ".concat(passwordStrength)
  }, passwordStrength && passwordStrength !== "blank" ? "Password Strength: " + passwordStrength : "Password Strength"), _react["default"].createElement("input", {
    type: "hidden",
    className: "gform_hidden",
    id: "input_".concat(formId, "_").concat(id, "_strength"),
    name: "input_".concat(id, "_strength"),
    value: passwordStrength
  })));
};

exports["default"] = _default;