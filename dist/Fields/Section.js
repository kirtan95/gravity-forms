"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _default = function _default(_ref) {
  var field = _ref.field,
      hideField = _ref.hideField,
      styledComponents = _ref.styledComponents;
  var cssClass = field.cssClass,
      width = field.width,
      label = field.label,
      description = field.description,
      descriptionPlacement = field.descriptionPlacement;

  var _ref2 = styledComponents || false,
      _ref2$Box = _ref2.Box,
      Box = _ref2$Box === void 0 ? 'div' : _ref2$Box;

  return _react["default"].createElement(Box, {
    width: width,
    className: "form-field gsection ".concat(cssClass),
    style: {
      display: hideField ? 'none' : undefined
    }
  }, _react["default"].createElement(_react["default"].Fragment, null, descriptionPlacement === 'above' && description && description && _react["default"].createElement("div", {
    className: "description"
  }, description), _react["default"].createElement("h2", {
    className: "gsection_title",
    dangerouslySetInnerHTML: {
      __html: label
    }
  }), descriptionPlacement !== 'above' && description && description && _react["default"].createElement("div", {
    className: "description"
  }, description)));
};

exports["default"] = _default;