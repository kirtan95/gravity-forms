"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireDefault(require("react"));

var _InputLabel = _interopRequireDefault(require("../FormElements/InputLabel"));

var _default = function _default(_ref) {
  var field = _ref.field,
      value = _ref.value,
      validationMessage = _ref.validationMessage,
      touched = _ref.touched,
      setTouched = _ref.setTouched,
      hideField = _ref.hideField,
      updateForm = _ref.updateForm,
      styledComponents = _ref.styledComponents,
      error = _ref.error,
      unsetError = _ref.unsetError,
      setFocusClass = _ref.setFocusClass,
      cssClass = _ref.cssClass,
      props = (0, _objectWithoutProperties2["default"])(_ref, ["field", "value", "validationMessage", "touched", "setTouched", "hideField", "updateForm", "styledComponents", "error", "unsetError", "setFocusClass", "cssClass"]);
  var id = field.id,
      formId = field.formId,
      type = field.type,
      label = field.label,
      placeholder = field.placeholder,
      isRequired = field.isRequired,
      maxLength = field.maxLength,
      description = field.description,
      descriptionPlacement = field.descriptionPlacement,
      labelPlacement = field.labelPlacement,
      width = field.width,
      customName = field.customName;

  var _ref2 = styledComponents || false,
      _ref2$Input = _ref2.Input,
      Input = _ref2$Input === void 0 ? 'input' : _ref2$Input,
      _ref2$SubLabel = _ref2.SubLabel,
      SubLabel = _ref2$SubLabel === void 0 ? 'label' : _ref2$SubLabel,
      _ref2$Box = _ref2.Box,
      Box = _ref2$Box === void 0 ? 'div' : _ref2$Box;

  var _ref3 = props || {},
      i18n = _ref3.i18n;

  var setDisabled = function setDisabled() {
    return cssClass === 'field--street' || cssClass === 'field--city';
  };

  return _react["default"].createElement(Box, {
    width: width,
    className: validationMessage && touched || error ? "form-field error ".concat(cssClass) : "form-field ".concat(cssClass),
    style: {
      display: hideField ? 'none' : undefined
    }
  }, _react["default"].createElement("div", {
    className: type
  }, _react["default"].createElement(_InputLabel["default"], {
    formId: formId,
    id: id,
    label: label,
    labelPlacement: labelPlacement,
    isRequired: isRequired,
    styledComponent: styledComponents
  }), descriptionPlacement === 'above' && description && _react["default"].createElement("div", {
    className: "description"
  }, description), _react["default"].createElement(Input, {
    id: "input_".concat(formId, "_").concat(id),
    name: customName || "input_".concat(id),
    type: type,
    value: !value ? '' : value,
    placeholder: placeholder,
    maxLength: maxLength,
    required: isRequired,
    onChange: function onChange(event) {
      updateForm(event, field);
      unsetError(id);
    },
    onBlur: function onBlur(event) {
      updateForm(event, field);
      setTouched(id);
      setFocusClass(value !== '');
    },
    disabled: setDisabled(),
    onFocus: function onFocus() {
      return setFocusClass(true);
    },
    "aria-label": label,
    "aria-describedby": "error_".concat(formId, "_").concat(id),
    "aria-invalid": !!validationMessage && touched || !!error
  }), maxLength && maxLength > 0 && _react["default"].createElement(SubLabel, {
    className: "charleft"
  }, i18n ? "".concat(i18n.t('maxCharachters', {
    length: value.length || 0,
    maxLength: maxLength
  })) : "".concat(value.length || 0, " of ").concat(maxLength, " max charachters")), descriptionPlacement !== 'above' && description && _react["default"].createElement("div", {
    className: "description"
  }, description), (validationMessage && touched || error) && _react["default"].createElement("span", {
    className: "error-message",
    id: "error_".concat(formId, "_").concat(id)
  }, validationMessage || error)));
};

exports["default"] = _default;