"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _default = function _default(_ref) {
  var Button = _ref.Button,
      Loading = _ref.Loading,
      text = _ref.text,
      showLoading = _ref.showLoading,
      className = _ref.className,
      isDisabled = _ref.isDisabled,
      icon = _ref.icon;
  var SButton = Button ? Button : "button";
  return _react["default"].createElement(SButton, {
    type: "submit",
    className: "form-submit-button button ".concat(className),
    disabled: showLoading || isDisabled,
    mr: 20
  }, showLoading && Loading ? _react["default"].createElement(Loading, {
    inline: true
  }) : text, text);
};

exports["default"] = _default;