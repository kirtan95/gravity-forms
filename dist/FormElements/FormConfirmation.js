"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _react = _interopRequireDefault(require("react"));

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = (0, _getPrototypeOf2["default"])(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = (0, _getPrototypeOf2["default"])(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return (0, _possibleConstructorReturn2["default"])(this, result); }; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

var Confirmation = function (_React$Component) {
  (0, _inherits2["default"])(Confirmation, _React$Component);

  var _super = _createSuper(Confirmation);

  function Confirmation() {
    var _this;

    (0, _classCallCheck2["default"])(this, Confirmation);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _super.call.apply(_super, [this].concat(args));
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "state", {
      confirmation: ''
    });
    return _this;
  }

  (0, _createClass2["default"])(Confirmation, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var confirmation = this.props.confirmation;
      var extractscript = /<script>(.+)<\/script>/gi.exec(confirmation);

      if (extractscript) {
        confirmation = confirmation.replace(extractscript[0], '');
        this.setState({
          confirmation: confirmation
        }, function () {
          return window.eval(extractscript[1]);
        });
      } else {
        this.setState({
          confirmation: confirmation
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var confirmation = this.state.confirmation;
      var SFormConfirmation = this.props.SFormConfirmation;
      return SFormConfirmation ? _react["default"].createElement(SFormConfirmation, {
        success: true,
        message: confirmation
      }) : _react["default"].createElement("div", {
        className: "form-confirmation",
        dangerouslySetInnerHTML: {
          __html: confirmation
        }
      });
    }
  }]);
  return Confirmation;
}(_react["default"].Component);

var _default = Confirmation;
exports["default"] = _default;