"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var FormError = function FormError(_ref) {
  var errorMessage = _ref.errorMessage,
      SFormError = _ref.SFormError;
  var Wrapper = SFormError || 'div';
  return _react["default"].createElement(Wrapper, null, _react["default"].createElement("div", {
    className: "form-error"
  }, errorMessage));
};

FormError.propTypes = {
  errorMessage: _propTypes["default"].string.isRequired
};
var _default = FormError;
exports["default"] = _default;