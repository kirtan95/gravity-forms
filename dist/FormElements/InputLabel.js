"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var InputLabel = function InputLabel(_ref) {
  var formId = _ref.formId,
      id = _ref.id,
      label = _ref.label,
      isRequired = _ref.isRequired,
      labelPlacement = _ref.labelPlacement,
      styledComponent = _ref.styledComponent;

  var _ref2 = styledComponent || false,
      _ref2$Label = _ref2.Label,
      Label = _ref2$Label === void 0 ? 'label' : _ref2$Label;

  return _react["default"].createElement(Label, {
    htmlFor: "input_".concat(formId, "_").concat(id),
    className: "gf-label ".concat(labelPlacement),
    style: {
      display: labelPlacement === 'hidden_label' ? 'none' : undefined
    }
  }, label, isRequired ? _react["default"].createElement("abbr", null, "*") : null);
};

var _default = InputLabel;
exports["default"] = _default;