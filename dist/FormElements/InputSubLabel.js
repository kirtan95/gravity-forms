"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var InputSubLabel = function InputSubLabel(_ref) {
  var formId = _ref.formId,
      id = _ref.id,
      label = _ref.label,
      labelPlacement = _ref.labelPlacement,
      styledComponent = _ref.styledComponent;

  var _ref2 = styledComponent || false,
      _ref2$SubLabel = _ref2.SubLabel,
      SubLabel = _ref2$SubLabel === void 0 ? 'label' : _ref2$SubLabel;

  return _react["default"].createElement(SubLabel, {
    htmlFor: "input_".concat(formId, "_").concat(id),
    className: "gf-label ".concat(labelPlacement),
    style: {
      display: labelPlacement === 'hidden_label' ? 'none' : undefined
    }
  }, label);
};

var _default = InputSubLabel;
exports["default"] = _default;