"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _default = function _default(_ref) {
  var pagination = _ref.pagination,
      activePage = _ref.activePage,
      firstPageCssClass = _ref.firstPageCssClass;
  var value;
  var pages = pagination.pages,
      type = pagination.type;

  switch (type) {
    case 'percentage':
      value = "".concat(100 / pages.length * activePage, "%");
      break;

    case 'steps':
      value = pages.join(' ');
      break;
  }

  return _react["default"].createElement("div", null, type === 'percentage' && firstPageCssClass === 'progress-bar' ? _react["default"].createElement("progress", {
    max: "100",
    value: 100 / pages.length * activePage
  }) : value);
};

exports["default"] = _default;