"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireWildcard(require("react"));

var FormFields = _interopRequireWildcard(require("../Fields"));

var frac2dec = function frac2dec(fraction) {
  var fractionParts = fraction.split('-');

  if (fractionParts.length === 1) {
    fractionParts = fraction.split(' ');
  }

  if (fractionParts.length > 1 && fraction.indexOf('/') !== -1) {
    var integer = parseInt(fractionParts[0]);
    var decimalParts = fractionParts[1].split('/');
    var decimal = parseInt(decimalParts[0]) / parseInt(decimalParts[1]);
    return integer + decimal;
  }

  if (fraction.indexOf('/') !== -1) {
    var _decimalParts = fraction.split('/');

    var _decimal = parseInt(_decimalParts[0]) / parseInt(_decimalParts[1]);

    return _decimal;
  }

  return parseInt(fraction);
};

var formatComponentName = function formatComponentName(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

var formatWidthFromCss = function formatWidthFromCss(cssClass) {
  if (!cssClass) return {};
  var widthStarts = cssClass.indexOf('[');
  var widthEnds = cssClass.indexOf(']');

  if (widthStarts === -1 || widthEnds === -1) {
    return {};
  }

  var width = cssClass.substring(widthStarts + 1, widthEnds).split(',').map(function (item) {
    return frac2dec(item.replace(/\s/g, ''));
  });
  var cleanedCssClass = "".concat(cssClass.replace(cssClass.substring(widthStarts, widthEnds + 1), ''), " custom-width");
  return {
    width: width,
    cleanedCssClass: cleanedCssClass
  };
};

var RenderField = function RenderField(_ref) {
  var field = _ref.field,
      formValues = _ref.formValues,
      submitFailed = _ref.submitFailed,
      submitSuccess = _ref.submitSuccess,
      setTouched = _ref.setTouched,
      setErrorMessages = _ref.setErrorMessages,
      touched = _ref.touched,
      _updateForm = _ref.updateForm,
      pages = _ref.pages,
      prevStep = _ref.prevStep,
      nextStep = _ref.nextStep,
      isNextDisabled = _ref.isNextDisabled,
      saveStateToHtmlField = _ref.saveStateToHtmlField,
      styledComponents = _ref.styledComponents,
      customComponents = _ref.customComponents,
      error = _ref.error,
      unsetError = _ref.unsetError,
      dropzoneText = _ref.dropzoneText,
      language = _ref.language,
      apiKeys = _ref.apiKeys,
      props = (0, _objectWithoutProperties2["default"])(_ref, ["field", "formValues", "submitFailed", "submitSuccess", "setTouched", "setErrorMessages", "touched", "updateForm", "pages", "prevStep", "nextStep", "isNextDisabled", "saveStateToHtmlField", "styledComponents", "customComponents", "error", "unsetError", "dropzoneText", "language", "apiKeys"]);
  var FormComponent = FormFields[formatComponentName(field.type)];

  if (customComponents && (customComponents[field.id] || customComponents[field.cssClass])) {
    FormComponent = FormFields[formatComponentName('custom')];
  }

  var _formatWidthFromCss = formatWidthFromCss(field.cssClass),
      cleanedCssClass = _formatWidthFromCss.cleanedCssClass,
      width = _formatWidthFromCss.width;

  if (width) {
    field.cssClass = cleanedCssClass;
    field.width = width;
  }

  var value = formValues[field.id] ? formValues[field.id].value : field.defaultValue;

  var _useState = (0, _react.useState)("".concat(field.cssClass).concat(field.type === 'select' ? value.value && value.value !== '' ? ' filled' : '' : value && value !== '' ? ' filled' : '')),
      _useState2 = (0, _slicedToArray2["default"])(_useState, 2),
      fieldClassName = _useState2[0],
      setFieldClassName = _useState2[1];

  var setFocusClass = function setFocusClass(action) {
    if (action) {
      if (fieldClassName.indexOf(' filled') === -1) {
        setFieldClassName("".concat(fieldClassName, " filled"));
      }
    } else {
      setFieldClassName(fieldClassName.replace(' filled', ''));
    }
  };

  return _react["default"].createElement(FormComponent, (0, _extends2["default"])({
    key: "el-".concat(field.formId, "-").concat(field.id),
    field: field,
    value: value,
    updateForm: function updateForm(event, field, inputID) {
      return _updateForm(event, field, inputID);
    },
    validationMessage: formValues[field.id] ? formValues[field.id].valid : false,
    formValues: formValues,
    submitFailed: submitFailed,
    submitSuccess: submitSuccess,
    touched: touched[field.id],
    setTouched: setTouched,
    setErrorMessages: setErrorMessages,
    unsetError: unsetError,
    error: error,
    pages: pages,
    prevStep: prevStep,
    nextStep: nextStep,
    isNextDisabled: isNextDisabled,
    hideField: formValues[field.id] ? formValues[field.id].hideField : false,
    saveStateToHtmlField: field.type === 'html' && field.cssClass.indexOf('set-state') !== -1 ? {
      formValues: formValues,
      saveStateToHtmlField: saveStateToHtmlField
    } : false,
    styledComponents: styledComponents,
    cssClass: fieldClassName,
    setFocusClass: setFocusClass,
    component: customComponents && (customComponents[field.id] || customComponents[field.cssClass]),
    dropzoneText: dropzoneText,
    language: language,
    apiKeys: apiKeys
  }, props));
};

var _default = RenderField;
exports["default"] = _default;