"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _react = _interopRequireWildcard(require("react"));

var _RenderField = _interopRequireDefault(require("./RenderField"));

var divideFieldsIntoPages = function divideFieldsIntoPages(fields, pages) {
  var tmpFields = pages.map(function (item) {
    return [];
  });

  for (var i = 0; i < fields.length; i++) {
    var arr = tmpFields[fields[i].pageNumber];

    if (tmpFields[fields[i].pageNumber - 1]) {
      if (fields[i].type === 'page') {
        tmpFields[fields[i].pageNumber - 2].push(fields[i]);
      } else {
        tmpFields[fields[i].pageNumber - 1].push(fields[i]);
      }
    }
  }

  return tmpFields;
};

var getMaxFieldId = function getMaxFieldId(fields) {
  var max = 0;

  for (var i = 0; i < fields.length; i++) {
    if (parseInt(fields[i].id) > max) {
      max = parseInt(fields[i].id);
    }
  }

  return max + 1;
};

var fieldTypes = ['checkbox', 'email', 'hidden', 'html', 'number', 'phone', 'radio', 'select', 'multiselect', 'text', 'textarea', 'website', 'page', 'date', 'fileupload', 'consent', 'password', 'section', 'scustom', 'name', 'address', 'buckarooideal', 'postcode'];
var honeyPotLables = ['Name', 'Email', 'Phone', 'Comments'];
var honeypotLabel = honeyPotLables[Math.floor(Math.random() * Math.floor(4))];

var _default = function _default(props) {
  var fields = props.fields,
      formValues = props.formValues,
      updateForm = props.updateForm,
      submitFailed = props.submitFailed,
      submitSuccess = props.submitSuccess,
      touched = props.touched,
      setTouched = props.setTouched,
      setErrorMessages = props.setErrorMessages,
      pagination = props.pagination,
      activePage = props.activePage,
      prevStep = props.prevStep,
      nextStep = props.nextStep,
      isNextDisabled = props.isNextDisabled,
      checkConditionalLogic = props.checkConditionalLogic,
      saveStateToHtmlField = props.saveStateToHtmlField,
      enableHoneypot = props.enableHoneypot,
      styledComponents = props.styledComponents,
      customComponents = props.customComponents,
      unsetError = props.unsetError,
      errors = props.errors,
      dropzoneText = props.dropzoneText,
      pageClicked = props.pageClicked,
      language = props.language,
      apiKeys = props.apiKeys,
      rest = (0, _objectWithoutProperties2["default"])(props, ["fields", "formValues", "updateForm", "submitFailed", "submitSuccess", "touched", "setTouched", "setErrorMessages", "pagination", "activePage", "prevStep", "nextStep", "isNextDisabled", "checkConditionalLogic", "saveStateToHtmlField", "enableHoneypot", "styledComponents", "customComponents", "unsetError", "errors", "dropzoneText", "pageClicked", "language", "apiKeys"]);
  var dividedFields = pagination ? divideFieldsIntoPages(fields, pagination.pages) : undefined;
  var maxID = getMaxFieldId(fields);

  var _useState = (0, _react.useState)(''),
      _useState2 = (0, _slicedToArray2["default"])(_useState, 2),
      honeypotValue = _useState2[0],
      setHoneypotValue = _useState2[1];

  var prevSteptRef = (0, _react.useRef)();
  (0, _react.useEffect)(function () {
    prevSteptRef.current = activePage;
  });
  var prevCount = prevSteptRef.current;

  function renderFiled(field) {
    return _react["default"].createElement(_RenderField["default"], (0, _extends2["default"])({
      key: "".concat(field.formId, "-").concat(field.id),
      field: field,
      formValues: formValues,
      submitFailed: submitFailed,
      setTouched: setTouched,
      setErrorMessages: setErrorMessages,
      submitSuccess: submitSuccess,
      updateForm: updateForm,
      touched: touched,
      pages: pagination && pagination.pages.length,
      prevStep: prevStep,
      nextStep: nextStep,
      isNextDisabled: isNextDisabled,
      checkConditionalLogic: checkConditionalLogic,
      saveStateToHtmlField: saveStateToHtmlField,
      styledComponents: styledComponents,
      customComponents: customComponents,
      error: errors && errors[field.id] ? errors[field.id] : false,
      unsetError: unsetError,
      dropzoneText: dropzoneText,
      language: language,
      apiKeys: apiKeys
    }, rest));
  }

  return _react["default"].createElement("div", {
    className: "form-fields".concat(pagination && pagination.pages.length > 1 ? " hasPages ".concat(!pageClicked ? ' noPageClicked' : '') : '')
  }, pagination && pagination.pages.length > 1 ? pagination.pages.map(function (page, index) {
    return _react["default"].createElement("div", {
      className: "page".concat(activePage === index + 1 ? ' active' : '').concat(prevCount && index === prevCount && activePage !== index + 1 && prevCount !== activePage ? ' prevStep' : ''),
      key: "page-".concat(index)
    }, page && _react["default"].createElement("div", {
      className: "gf_step"
    }, _react["default"].createElement("span", null, page)), dividedFields[index].map(function (field) {
      return fieldTypes.includes(field.type) && renderFiled(field);
    }));
  }) : fields.map(function (field) {
    return fieldTypes.includes(field.type) && renderFiled(field);
  }), enableHoneypot && _react["default"].createElement("div", {
    className: "form-field gform_validation_container"
  }, _react["default"].createElement("label", {
    htmlFor: "input_".concat(maxID),
    className: "gf-label "
  }, honeypotLabel), _react["default"].createElement("input", {
    type: "text",
    name: "input_".concat(maxID),
    id: "input_".concat(maxID),
    value: honeypotValue,
    onChange: function onChange(e) {
      return setHoneypotValue(e.target.value);
    },
    autoComplete: "off"
  })));
};

exports["default"] = _default;