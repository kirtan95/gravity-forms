"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var Submit = function Submit(_ref) {
  var Button = _ref.Button,
      Loading = _ref.Loading,
      formData = _ref.formData,
      isDisabled = _ref.isDisabled,
      submitting = _ref.submitting,
      prevStep = _ref.prevStep,
      Component = _ref.Component,
      loadingSpinner = _ref.loadingSpinner;
  var SButton = Button || 'button';
  return _react["default"].createElement(_react["default"].Fragment, null, _react["default"].createElement("div", {
    className: "footer".concat(Component ? ' multiple' : '')
  }, _react["default"].createElement("input", {
    type: "hidden",
    name: "nonce",
    value: formData.nonce
  }), Component ? _react["default"].createElement(Component, {
    submitting: submitting
  }) : '', _react["default"].createElement(SButton, {
    type: "submit",
    mr: 20,
    disabled: isDisabled || submitting,
    className: submitting ? 'loading' : undefined
  }, formData.button.text || 'Submit', loadingSpinner ? _react["default"].createElement("span", null) : ''), formData.lastPageButton && _react["default"].createElement(SButton, {
    className: "prev",
    onClick: prevStep
  }, formData.lastPageButton.text)), Loading && !loadingSpinner && _react["default"].createElement(Loading, {
    isLoading: submitting
  }));
};

var _default = Submit;
exports["default"] = _default;