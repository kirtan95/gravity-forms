"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var ValidationMessage = function ValidationMessage(_ref) {
  var validationMessage = _ref.validationMessage,
      formId = _ref.formId,
      id = _ref.id,
      error = _ref.error;
  return _react["default"].createElement("span", {
    className: "error-message",
    id: "error_".concat(formId, "_").concat(id)
  }, validationMessage || error);
};

ValidationMessage.propTypes = {
  validationMessage: _propTypes["default"].string.isRequired
};
var _default = ValidationMessage;
exports["default"] = _default;