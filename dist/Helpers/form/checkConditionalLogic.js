"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(condition) {
  var fields = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var rules = condition.rules,
      actionType = condition.actionType;
  if (!rules) return true;
  var formValues = fields || formValues;
  var hideField = actionType !== 'hide';
  var hideBasedOnRules = [];

  for (var i = 0; i < rules.length; i++) {
    var _rules$i = rules[i],
        fieldId = _rules$i.fieldId,
        value = _rules$i.value,
        operator = _rules$i.operator;
    var conditionFieldValue = formValues[fieldId].value && formValues[fieldId].value.value ? formValues[fieldId].value.value : formValues[fieldId].value || false;
    var stringValue = Array.isArray(conditionFieldValue) ? conditionFieldValue.join('') : conditionFieldValue;

    if (!value) {
      if (!stringValue && !value) {
        hideBasedOnRules[i] = actionType === 'hide';
      } else {
        hideBasedOnRules[i] = actionType !== 'hide';
      }
    } else if (stringValue && value === stringValue) {
      hideBasedOnRules[i] = actionType === 'hide';
    } else if (stringValue && stringValue.includes(value)) {
      hideBasedOnRules[i] = actionType === 'hide';
    } else {
      hideBasedOnRules[i] = actionType !== 'hide';
    }

    if (operator === 'isnot') {
      hideBasedOnRules[i] = !hideBasedOnRules[i];
    }
  }

  hideField = hideBasedOnRules.every(function (i) {
    return i === true;
  });
  return hideField;
};

exports["default"] = _default;