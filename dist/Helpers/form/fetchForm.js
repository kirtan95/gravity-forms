"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _taggedTemplateLiteral2 = _interopRequireDefault(require("@babel/runtime/helpers/taggedTemplateLiteral"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _isomorphicUnfetch = _interopRequireDefault(require("isomorphic-unfetch"));

var _index = require("./index");

var _validation = require("../validation");

var _checkConditionalLogic = _interopRequireDefault(require("./checkConditionalLogic"));

var _client = require("@apollo/client");

function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _templateObject() {
  var data = (0, _taggedTemplateLiteral2["default"])(["\n  query getForm($formId: ID!) {\n    gfForm(id: $formId, idType: DATABASE_ID) {\n      id\n      title\n      description\n      submitButton {\n        text\n        imageUrl\n        width\n      }\n      confirmations {\n        isDefault\n        message\n      }\n      pagination {\n        type\n        style\n        progressbarCompletionText\n        pageNames\n        hasProgressbarOnConfirmation\n        color\n        backgroundColor\n        lastPageButton {\n          imageUrl\n          text\n          type\n        }\n      }\n      formFields(first: 500) {\n        nodes {\n          id\n          type\n          pageNumber\n          ... on AddressField {\n            ...AddressFieldFields\n          }\n          ... on CheckboxField {\n            ...CheckboxFieldFields\n          }\n          ... on ConsentField {\n            ...ConsentFieldFields\n          }\n          ... on DateField {\n            ...DateFieldFields\n          }\n          ... on EmailField {\n            ...EmailFieldFields\n          }\n          ... on FileUploadField {\n            ...FileUploadFieldFields\n          }\n          ... on HiddenField {\n            ...HiddenFieldFields\n          }\n          ... on HtmlField {\n            ...HtmlFieldFields\n          }\n          ... on MultiSelectField {\n            ...MultiSelectFieldFields\n          }\n          ... on NameField {\n            ...NameFieldFields\n          }\n          ... on NumberField {\n            ...NumberFieldFields\n          }\n          ... on PageField {\n            ...PageFieldFields\n          }\n          ... on PasswordField {\n            ...PasswordFieldFields\n          }\n          ... on PhoneField {\n            ...PhoneFieldFields\n          }\n          ... on RadioField {\n            ...RadioFieldFields\n          }\n          ... on SectionField {\n            ...SectionFieldFields\n          }\n          ... on SelectField {\n            ...SelectFieldFields\n          }\n          ... on TextField {\n            ...TextFieldFields\n          }\n          ... on TextAreaField {\n            ...TextAreaFieldFields\n          }\n          ... on TimeField {\n            ...TimeFieldFields\n          }\n          ... on WebsiteField {\n            ...WebsiteFieldFields\n          }\n        }\n      }\n      formId\n      databaseId\n      firstPageCssClass\n      hasHoneypot\n      button {\n        text\n        type\n        location\n        imageUrl\n        width\n        conditionalLogic {\n          actionType\n          logicType\n          rules {\n            fieldId\n            operator\n            value\n          }\n        }\n      }\n      labelPlacement\n      descriptionPlacement\n    }\n  }\n  \n  fragment AddressFieldFields on AddressField {\n    label\n    description\n    cssClass\n    inputName\n    inputs {\n      defaultValue\n      autocompleteAttribute\n      customLabel\n      id\n      isHidden\n      key\n      label\n      name\n      placeholder\n    }\n    conditionalLogic {\n      rules {\n        fieldId\n        operator\n        value\n      }\n      actionType\n      logicType\n    }\n    inputs {\n      defaultValue\n      autocompleteAttribute\n      customLabel\n      id\n      isHidden\n      key\n      label\n      name\n      placeholder\n    }\n    errorMessage\n    isRequired\n    descriptionPlacement\n    labelPlacement\n    pageNumber\n    value\n  }\n  \n  fragment CheckboxFieldFields on CheckboxField {\n    inputName\n    label\n    isRequired\n    description\n    cssClass\n    conditionalLogic {\n      rules {\n        fieldId\n        operator\n        value\n      }\n      actionType\n      logicType\n    }\n    inputs {\n      id\n      label\n      name\n    }\n    choices {\n      text\n      value\n      isSelected\n    }\n    errorMessage\n    descriptionPlacement\n    labelPlacement\n    pageNumber\n    value\n  }\n  \n  fragment ConsentFieldFields on ConsentField {\n    label\n    cssClass\n    conditionalLogic {\n      actionType\n      logicType\n      rules {\n        fieldId\n        operator\n        value\n      }\n    }\n    isRequired\n    description\n    descriptionPlacement\n    labelPlacement\n    checkboxLabel\n    pageNumber\n    errorMessage\n    value\n  }\n  \n  fragment DateFieldFields on DateField {\n    inputName\n    label\n    description\n    cssClass\n    isRequired\n    placeholder\n    conditionalLogic {\n      rules {\n        fieldId\n        operator\n        value\n      }\n      actionType\n      logicType\n    }\n    defaultValue\n    errorMessage\n    inputs {\n      autocompleteAttribute\n      customLabel\n      defaultValue\n      id\n      label\n      placeholder\n    }\n    dateType\n    descriptionPlacement\n    labelPlacement\n    pageNumber\n    dateFormat\n    value\n  }\n  \n  fragment EmailFieldFields on EmailField {\n    label\n    description\n    cssClass\n    isRequired\n    placeholder\n    conditionalLogic {\n      rules {\n        fieldId\n        operator\n        value\n      }\n      actionType\n      logicType\n    }\n    errorMessage\n    hasEmailConfirmation\n    inputs {\n      autocompleteAttribute\n      customLabel\n      defaultValue\n      id\n      label\n      name\n      placeholder\n    }\n    descriptionPlacement\n    labelPlacement\n    pageNumber\n    value\n  }\n  \n  fragment FileUploadFieldFields on FileUploadField {\n    conditionalLogic {\n      actionType\n      logicType\n      rules {\n        fieldId\n        value\n        operator\n      }\n    }\n    cssClass\n    description\n    allowedExtensions\n    descriptionPlacement\n    errorMessage\n    isRequired\n    label\n    labelPlacement\n    maxFileSize\n    maxFiles\n    pageNumber\n    value\n    values\n  }\n  \n  fragment HiddenFieldFields on HiddenField {\n    label\n    pageNumber\n    inputName\n    defaultValue\n    value\n  }\n  \n  fragment HtmlFieldFields on HtmlField {\n    label\n    pageNumber\n    content\n    cssClass\n    conditionalLogic {\n      actionType\n      logicType\n      rules {\n        fieldId\n        operator\n        value\n      }\n    }\n  }\n  \n  fragment MultiSelectFieldFields on MultiSelectField {\n    inputName\n    label\n    description\n    cssClass\n    isRequired\n    choices {\n      text\n      value\n      isSelected\n    }\n    conditionalLogic {\n      rules {\n        fieldId\n        operator\n        value\n      }\n      actionType\n      logicType\n    }\n    errorMessage\n    descriptionPlacement\n    labelPlacement\n    pageNumber\n    value\n    values\n  }\n  \n  fragment NameFieldFields on NameField {\n    inputName\n    label\n    description\n    cssClass\n    isRequired\n    inputs {\n      key\n      label\n      placeholder\n      choices {\n        text\n        value\n        isSelected\n      }\n      defaultValue\n      autocompleteAttribute\n      customLabel\n      hasChoiceValue\n      id\n      isHidden\n      name\n    }\n    conditionalLogic {\n      rules {\n        fieldId\n        operator\n        value\n      }\n      actionType\n      logicType\n    }\n    errorMessage\n    descriptionPlacement\n    labelPlacement\n    pageNumber\n    value\n    nameValues {\n      first\n      last\n      middle\n      prefix\n      suffix\n    }\n  }\n  \n  fragment NumberFieldFields on NumberField {\n    label\n    labelPlacement\n    placeholder\n    pageNumber\n    isRequired\n    description\n    descriptionPlacement\n    cssClass\n    conditionalLogic {\n      actionType\n      logicType\n      rules {\n        fieldId\n        operator\n        value\n      }\n    }\n    inputName\n    defaultValue\n    errorMessage\n    value\n  }\n  \n  fragment PageFieldFields on PageField {\n    conditionalLogic {\n      actionType\n      logicType\n      rules {\n        fieldId\n        operator\n        value\n      }\n    }\n    cssClass\n    pageNumber\n    nextButton {\n      imageUrl\n      text\n      type\n      conditionalLogic {\n        actionType\n        logicType\n        rules {\n          fieldId\n          operator\n          value\n        }\n      }\n    }\n    previousButton {\n      imageUrl\n      text\n      type\n      conditionalLogic {\n        actionType\n        logicType\n        rules {\n          fieldId\n          operator\n          value\n        }\n      }\n    }\n  }\n  \n  fragment PasswordFieldFields on PasswordField {\n    conditionalLogic {\n      actionType\n      logicType\n      rules {\n        fieldId\n        operator\n        value\n      }\n    }\n    cssClass\n    description\n    descriptionPlacement\n    errorMessage\n    inputs {\n      customLabel\n      id\n      isHidden\n      label\n      placeholder\n    }\n    label\n    isRequired\n    labelPlacement\n    pageNumber\n    hasPasswordStrengthIndicator\n    minPasswordStrength\n    value\n  }\n  \n  fragment PhoneFieldFields on PhoneField {\n    inputName\n    label\n    description\n    cssClass\n    isRequired\n    placeholder\n    conditionalLogic {\n      rules {\n        fieldId\n        operator\n        value\n      }\n      actionType\n      logicType\n    }\n    defaultValue\n    errorMessage\n    descriptionPlacement\n    labelPlacement\n    pageNumber\n    value\n  }\n  \n  fragment RadioFieldFields on RadioField {\n    inputName\n    label\n    isRequired\n    description\n    cssClass\n    choices {\n      text\n      value\n      isSelected\n      isOtherChoice\n    }\n    conditionalLogic {\n      rules {\n        fieldId\n        operator\n        value\n      }\n      actionType\n      logicType\n    }\n    errorMessage\n    descriptionPlacement\n    labelPlacement\n    pageNumber\n    hasOtherChoice\n    value\n  }\n  \n  fragment SectionFieldFields on SectionField {\n    cssClass\n    description\n    label\n    pageNumber\n    conditionalLogic {\n      actionType\n      logicType\n      rules {\n        fieldId\n        operator\n        value\n      }\n    }\n  }\n  \n  fragment SelectFieldFields on SelectField {\n    label\n    description\n    cssClass\n    isRequired\n    defaultValue\n    choices {\n      text\n      value\n      isSelected\n    }\n    conditionalLogic {\n      rules {\n        fieldId\n        operator\n        value\n      }\n      actionType\n      logicType\n    }\n    inputName\n    errorMessage\n    placeholder\n    descriptionPlacement\n    labelPlacement\n    pageNumber\n    value\n  }\n  \n  fragment TextAreaFieldFields on TextAreaField {\n    label\n    description\n    cssClass\n    isRequired\n    conditionalLogic {\n      rules {\n        fieldId\n        operator\n        value\n      }\n      actionType\n      logicType\n    }\n    defaultValue\n    inputName\n    errorMessage\n    placeholder\n    maxLength\n    descriptionPlacement\n    labelPlacement\n    pageNumber\n    value\n  }\n  \n  fragment TextFieldFields on TextField {\n    inputName\n    label\n    description\n    cssClass\n    isRequired\n    placeholder\n    conditionalLogic {\n      rules {\n        fieldId\n        operator\n        value\n      }\n      actionType\n      logicType\n    }\n    defaultValue\n    errorMessage\n    maxLength\n    descriptionPlacement\n    labelPlacement\n    pageNumber\n    value\n  }\n  \n  fragment TimeFieldFields on TimeField {\n    label\n    description\n    cssClass\n    isRequired\n    conditionalLogic {\n      rules {\n        fieldId\n        operator\n        value\n      }\n      actionType\n      logicType\n    }\n    errorMessage\n    inputs {\n      id\n      label\n      placeholder\n      defaultValue\n      customLabel\n      autocompleteAttribute\n    }\n    descriptionPlacement\n    labelPlacement\n    pageNumber\n    value\n  }\n  \n  fragment WebsiteFieldFields on WebsiteField {\n    inputName\n    label\n    description\n    cssClass\n    isRequired\n    placeholder\n    conditionalLogic {\n      rules {\n        fieldId\n        operator\n        value\n      }\n      actionType\n      logicType\n    }\n    defaultValue\n    errorMessage\n    descriptionPlacement\n    labelPlacement\n    pageNumber\n    value\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function fetchFromRestEndpoint(_x, _x2, _x3, _x4) {
  return _fetchFromRestEndpoint.apply(this, arguments);
}

function _fetchFromRestEndpoint() {
  _fetchFromRestEndpoint = (0, _asyncToGenerator2["default"])(_regenerator["default"].mark(function _callee(getParams, backendUrl, formID, fetchOptions) {
    var queryString, requestUrl, form;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            queryString = getParams ? Object.keys(getParams).map(function (key) {
              return "".concat(key, "=").concat(getParams[key]);
            }).join('&') : '';
            requestUrl = "".concat(backendUrl, "/").concat(formID).concat(queryString ? "?".concat(queryString) : '');
            _context.next = 4;
            return (0, _isomorphicUnfetch["default"])(requestUrl, fetchOptions).then(function (resp) {
              return resp.json();
            }).then(function (response) {
              return response;
            })["catch"](function () {
              return false;
            });

          case 4:
            form = _context.sent;
            return _context.abrupt("return", form);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _fetchFromRestEndpoint.apply(this, arguments);
}

function fetchFromGraphQLEndpoint(_x5, _x6, _x7, _x8, _x9) {
  return _fetchFromGraphQLEndpoint.apply(this, arguments);
}

function _fetchFromGraphQLEndpoint() {
  _fetchFromGraphQLEndpoint = (0, _asyncToGenerator2["default"])(_regenerator["default"].mark(function _callee2(client, getParams, backendUrl, formID, fetchOptions) {
    var _result$data;

    var GET_FORM, result;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            GET_FORM = (0, _client.gql)(_templateObject());
            _context2.next = 3;
            return client.query({
              query: GET_FORM,
              variables: {
                formId: formID
              }
            });

          case 3:
            result = _context2.sent;
            return _context2.abrupt("return", mapGraphQLResponsetoREST(result === null || result === void 0 ? void 0 : (_result$data = result.data) === null || _result$data === void 0 ? void 0 : _result$data.gfForm));

          case 6:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _fetchFromGraphQLEndpoint.apply(this, arguments);
}

function mapGraphQLResponsetoREST(data) {
  var _data, _data$pagination;

  if (!data) return data;
  data = _objectSpread(_objectSpread({}, data), {}, {
    formId: data.databaseId
  });
  data.fields = data.formFields.nodes.map(function (field) {
    var _field, _field2, _field3, _field4, _field5, _field6, _field7, _field8, _field9, _field10, _field11;

    field = _objectSpread(_objectSpread({}, field), {}, {
      type: field.type.toLowerCase()
    });

    if (((_field = field) === null || _field === void 0 ? void 0 : _field.cssClass) === null) {
      field.cssClass = "";
    }

    if (((_field2 = field) === null || _field2 === void 0 ? void 0 : _field2.defaultValue) === null) {
      field.defaultValue = "";
    }

    if (((_field3 = field) === null || _field3 === void 0 ? void 0 : _field3.inputName) === null) {
      field.inputName = "";
    }

    if (((_field4 = field) === null || _field4 === void 0 ? void 0 : _field4.placeholder) === null) {
      field.placeholder = "";
    }

    if (((_field5 = field) === null || _field5 === void 0 ? void 0 : _field5.conditionalLogic) === null) {
      field.conditionalLogic = "";
    }

    if (((_field6 = field) === null || _field6 === void 0 ? void 0 : _field6.value) === null) {
      field.value = "";
    }

    if (((_field7 = field) === null || _field7 === void 0 ? void 0 : _field7.defaultValue) === null) {
      field.defaultValue = "";
    }

    if (((_field8 = field) === null || _field8 === void 0 ? void 0 : _field8.errorMessage) === null) {
      field.errorMessage = "";
    }

    if (((_field9 = field) === null || _field9 === void 0 ? void 0 : _field9.descriptionPlacement) === "INHERIT") {
      field.descriptionPlacement = "";
    }

    if (((_field10 = field) === null || _field10 === void 0 ? void 0 : _field10.maxLength) === 0) {
      field.maxLength = "";
    }

    if ('hasPasswordStrengthIndicator' in field) {
      field.passwordStrengthEnabled = field.hasPasswordStrengthIndicator;
    }

    if ((_field11 = field) === null || _field11 === void 0 ? void 0 : _field11.inputs) {
      field.inputs = field.inputs.map(function (input) {
        return _objectSpread(_objectSpread({}, input), {}, {
          cssClass: "",
          defaultValue: input.defaultValue === null ? '' : input.defaultValue
        });
      });
    }

    if (field.type === 'name') {
      field.defaultValue = "";
      field.inputs = field.inputs.map(function (input) {
        if (input.choices !== null) {
          input.inputType = 'radio';
        }

        return input;
      });
    }

    if (field.type === 'address') {
      field.placeholder = "";
      field.maxLength = "";
    }

    field.formId = data.databaseId;

    if ('hasEmailConfirmation' in field) {
      field.emailConfirmEnabled = field.hasEmailConfirmation;
    }

    return field;
  });

  if ((_data = data) === null || _data === void 0 ? void 0 : (_data$pagination = _data.pagination) === null || _data$pagination === void 0 ? void 0 : _data$pagination.pageNames) {
    data.pagination = _objectSpread(_objectSpread({}, data.pagination), {}, {
      pages: data.pagination.pageNames
    });
  }

  if (!'maxLength' in data) {
    data.maxLength = "";
  }

  if ('hasHoneypot' in data) {
    data.enableHoneypot = data.hasHoneypot;
  }

  return data;
}

function fetchForm(_x10) {
  return _fetchForm.apply(this, arguments);
}

function _fetchForm() {
  _fetchForm = (0, _asyncToGenerator2["default"])(_regenerator["default"].mark(function _callee3(_ref) {
    var client, initialPage, fetchOptions, setFormData, setFormValues, setActivePage, setConditionFields, setConditionalIds, setPages, setIsMultiPart, populatedFields, populatedEntry, getParams, backendUrl, formID, apiType, isMultipart, params, form, formValues, conditionFields, conditionalIds, pages, _iterator, _step, field, value, tmpField, ids, _i, id, i;

    return _regenerator["default"].wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            client = _ref.client, initialPage = _ref.initialPage, fetchOptions = _ref.fetchOptions, setFormData = _ref.setFormData, setFormValues = _ref.setFormValues, setActivePage = _ref.setActivePage, setConditionFields = _ref.setConditionFields, setConditionalIds = _ref.setConditionalIds, setPages = _ref.setPages, setIsMultiPart = _ref.setIsMultiPart, populatedFields = _ref.populatedFields, populatedEntry = _ref.populatedEntry, getParams = _ref.getParams, backendUrl = _ref.backendUrl, formID = _ref.formID, apiType = _ref.apiType;
            isMultipart = false;
            params = [getParams, backendUrl, formID, fetchOptions];

            if (!(apiType === 'graphql')) {
              _context3.next = 9;
              break;
            }

            _context3.next = 6;
            return fetchFromGraphQLEndpoint.apply(void 0, [client].concat(params));

          case 6:
            _context3.t0 = _context3.sent;
            _context3.next = 12;
            break;

          case 9:
            _context3.next = 11;
            return fetchFromRestEndpoint.apply(void 0, params);

          case 11:
            _context3.t0 = _context3.sent;

          case 12:
            form = _context3.t0;

            if (form) {
              formValues = {};
              conditionFields = [];
              conditionalIds = [];
              pages = [];
              _iterator = _createForOfIteratorHelper(form.fields);

              try {
                for (_iterator.s(); !(_step = _iterator.n()).done;) {
                  field = _step.value;
                  value = void 0;

                  if (field.type === 'page') {
                    pages.push(field.id);
                  }

                  value = (0, _index.getFieldPrepopulatedValue)(field, populatedFields, populatedEntry);

                  if (field.type === 'fileupload') {
                    isMultipart = true;
                  }

                  if (field.conditionalLogic) {
                    tmpField = {
                      id: field.id,
                      conditionalLogic: field.conditionalLogic
                    };
                    ids = field.conditionalLogic.rules.map(function (item) {
                      return item.fieldId;
                    });

                    for (_i = 0; _i < ids.length; _i++) {
                      id = parseInt(ids[_i]);

                      if (conditionalIds.indexOf(id) === -1) {
                        conditionalIds.push(id);
                      }
                    }

                    conditionFields.push(tmpField);
                  }

                  formValues[field.id] = (0, _defineProperty2["default"])({
                    valid: (0, _validation.validateField)(value, field),
                    value: value,
                    type: field.type,
                    label: field.label,
                    pageNumber: field.pageNumber,
                    cssClass: field.cssClass,
                    isRequired: field.isRequired
                  }, "type", field.type);
                }
              } catch (err) {
                _iterator.e(err);
              } finally {
                _iterator.f();
              }

              for (i = 0; i < conditionFields.length; i++) {
                formValues[conditionFields[i].id].hideField = (0, _checkConditionalLogic["default"])(conditionFields[i].conditionalLogic, formValues);
              }

              setFormData(form);
              setFormValues(formValues);
              setActivePage(initialPage || (form.pagination ? 1 : false));
              setConditionFields(conditionFields);
              setConditionalIds(conditionalIds);
              if (isMultipart) setIsMultiPart(isMultipart);
              if (!!pages.length) setPages(pages);
            }

          case 14:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _fetchForm.apply(this, arguments);
}

var _default = fetchForm;
exports["default"] = _default;