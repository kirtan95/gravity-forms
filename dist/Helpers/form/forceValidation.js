"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var forceValidation = function forceValidation(page, formValues, setShowPageValidationMsg, setTouched) {
  var notValid = page ? Object.keys(formValues).some(function (x) {
    return formValues[x].pageNumber === page && !formValues[x].hideField && formValues[x].valid;
  }) : Object.keys(formValues).some(function (x) {
    return !formValues[x].hideField && formValues[x].valid;
  });

  if (notValid) {
    var fields = page ? Object.keys(formValues).filter(function (x) {
      return formValues[x].pageNumber === page;
    }) : Object.keys(formValues);
    setTouchedFields(fields, setShowPageValidationMsg, setTouched);
    return false;
  }

  return true;
};

function setTouchedFields(fields, setShowPageValidationMsg, setTouched) {
  var currentPageTouched = fields.reduce(function (currentTouched, x) {
    currentTouched = _objectSpread(_objectSpread({}, currentTouched), {}, (0, _defineProperty2["default"])({}, x, true));
    return currentTouched;
  }, {});
  setShowPageValidationMsg(true);
  setTouched(currentPageTouched);
}

var _default = forceValidation;
exports["default"] = _default;