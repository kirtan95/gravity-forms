"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var getNextStep = function getNextStep(activePage, pages, formValues) {
  var nextPage = activePage + 1;
  var nextPageId = pages[activePage - 1];

  if (!formValues[nextPageId]) {
    return false;
  }

  if (formValues[nextPageId].hideField === true) {
    nextPage = getNextStep(nextPage);
  }

  return nextPage;
};

var _default = getNextStep;
exports["default"] = _default;