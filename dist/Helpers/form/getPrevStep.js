"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var getPrevStep = function getPrevStep(activePage, pages, formValues) {
  var prevPage = activePage - 1;
  var prevPageId = pages[activePage - 3] || 0;

  if (formValues[prevPageId] && formValues[prevPageId].hideField === true) {
    prevPage = getPrevStep(prevPage);
  }

  return prevPage;
};

var _default = getPrevStep;
exports["default"] = _default;