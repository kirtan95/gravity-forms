"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _forceValidation = _interopRequireDefault(require("./forceValidation"));

var _getNextStep = _interopRequireDefault(require("./getNextStep"));

var _default = function _default(event, props, pages, formValues, activePage, setActivePage, setPageClicked, setTouched, setShowPageValidationMsg) {
  event.preventDefault();
  var beforeNextPage = props.activePage;
  var isPageValidated = (0, _forceValidation["default"])(activePage, formValues, setShowPageValidationMsg, setTouched);
  if (!isPageValidated) return false;
  var nextPage = (0, _getNextStep["default"])(activePage, pages, formValues);

  if (beforeNextPage) {
    beforeNextPage(activePage, formValues, nextPage);
  }

  setActivePage(nextPage);
  setPageClicked(true);
  setShowPageValidationMsg(false);
  setActivePage(nextPage);
  setPageClicked(true);
  setShowPageValidationMsg(false);
};

exports["default"] = _default;