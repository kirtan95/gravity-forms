"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _isomorphicUnfetch = _interopRequireDefault(require("isomorphic-unfetch"));

var _index = require("./index");

function onSubmit(_x, _x2, _x3, _x4, _x5, _x6, _x7, _x8, _x9, _x10, _x11, _x12, _x13) {
  return _onSubmit.apply(this, arguments);
}

function _onSubmit() {
  _onSubmit = (0, _asyncToGenerator2["default"])(_regenerator["default"].mark(function _callee(event, props, formValues, activePage, wrapperRef, setSubmitting, submitSuccess, setSubmitSuccess, setSubmitFailed, setShowPageValidationMsg, setTouched, setConfirmationMessage, setErrorMessages) {
    var customOnSubmit, formData, isPageValid, formID, backendUrl, jumpToConfirmation, onSubmitSuccess, onError, gfSubmissionUrl;
    return _regenerator["default"].wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            customOnSubmit = props.onSubmit;
            formData = new FormData(event.target);
            event.preventDefault();
            isPageValid = (0, _index.forceValidation)(activePage, formValues, setShowPageValidationMsg, setTouched);

            if (isPageValid) {
              _context.next = 6;
              break;
            }

            return _context.abrupt("return", false);

          case 6:
            if (customOnSubmit) {
              customOnSubmit(formData);
            } else {
              setSubmitting(true);
              setSubmitSuccess(false);
              setSubmitFailed(false);
              setConfirmationMessage(false);
              setErrorMessages(false);
              formID = props.formID, backendUrl = props.backendUrl, jumpToConfirmation = props.jumpToConfirmation, onSubmitSuccess = props.onSubmitSuccess, onError = props.onError;
              gfSubmissionUrl = backendUrl.substring(0, backendUrl.indexOf('/wp-json'));
              (0, _isomorphicUnfetch["default"])("".concat(gfSubmissionUrl, "/wp-json/gf/v2/forms/").concat(formID, "/submissions"), {
                method: 'POST',
                body: formData
              }).then(function (resp) {
                return resp.json();
              }).then(function (response) {
                console.log(response);

                if (response && response.is_valid) {
                  console.log('valid');

                  if (onSubmitSuccess) {
                    var res = onSubmitSuccess(response);

                    if (!res) {
                      return false;
                    }
                  }

                  var confirmationMessage = response.confirmation_message;

                  var _ref = confirmationMessage || false,
                      type = _ref.type,
                      link = _ref.link;

                  if (type && link && type === 'redirect') {
                    if (typeof window !== 'undefined') {
                      window.location.replace(link);
                      return false;
                    }
                  }

                  setSubmitting(false);
                  setSubmitSuccess(true);
                  setConfirmationMessage(confirmationMessage);
                  console.log(confirmationMessage);
                  console.log(submitSuccess);

                  if (jumpToConfirmation) {
                    (0, _index.scrollToConfirmation)(props, wrapperRef);
                  }
                } else {
                  throw {
                    response: response
                  };
                }
              })["catch"](function (error) {
                var errorMessages = error && error.response && error.response.validation_messages ? error.response.validation_messages : 'Something went wrong';

                if (onError) {
                  onError(errorMessages);
                  setSubmitting(false);
                  setSubmitFailed(true);
                } else {
                  setSubmitting(false);
                  setSubmitFailed(true);
                  setErrorMessages(errorMessages);
                }

                if (jumpToConfirmation) {
                  (0, _index.scrollToConfirmation)(props, wrapperRef);
                }
              });
            }

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _onSubmit.apply(this, arguments);
}

var _default = onSubmit;
exports["default"] = _default;