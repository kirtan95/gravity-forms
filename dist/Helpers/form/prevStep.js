"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _getPrevStep = _interopRequireDefault(require("./getPrevStep"));

var _default = function _default(formValues, pages, activePage, setActivePage, setPageClicked) {
  var prevPage = (0, _getPrevStep["default"])(activePage, pages, formValues) || 1;
  setActivePage(prevPage);
  setPageClicked(true);
};

exports["default"] = _default;