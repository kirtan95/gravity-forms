"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _default = function _default(props, wrapperRef) {
  var offset = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
  var onChangePage = props.onChangePage,
      jumpToConfirmation = props.jumpToConfirmation;

  if (onChangePage) {
    onChangePage();
  }

  if (jumpToConfirmation) {
    var rect = wrapperRef ? wrapperRef.current.getBoundingClientRect() : false;

    if (rect && window) {
      var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      window.scrollTo({
        top: scrollTop + rect.top - 100 - offset
      });
    }
  }
};

exports["default"] = _default;