"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var unsetError = function unsetError(id, errorMessages) {
  if (!errorMessages) return;

  if (errorMessages) {
    if ((0, _typeof2["default"])(errorMessages) === 'object' && errorMessages[id]) {
      delete errorMessages[id];
    }
  }
};

var _default = unsetError;
exports["default"] = _default;
unsetError.propTypes = {
  id: _propTypes["default"].number.isRequired,
  errorMessages: _propTypes["default"].func
};