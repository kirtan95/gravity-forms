"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _index = require("./index");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var _default = function _default(populatedEntry, formValues, formData, setFormValues) {
  var keys = Object.keys(formValues);
  var changed = false;

  var tmpValues = _objectSpread({}, formValues);

  var _loop = function _loop(i) {
    var id = keys[i];

    if (populatedEntry[id]) {
      var field = formData.fields.filter(function (item) {
        return item.id == id;
      });

      if (!field[0]) {
        return "continue";
      }

      var value = (0, _index.getFieldPrepopulatedValue)(field[0], [], populatedEntry);

      if (value && formValues[id] && formValues[id].value && formValues[id].value != value) {
        tmpValues[id].value = value;
        changed = true;
      }
    }
  };

  for (var i = 0; i < keys.length; i++) {
    var _ret = _loop(i);

    if (_ret === "continue") continue;
  }

  if (changed) {
    setFormValues(tmpValues);
  }
};

exports["default"] = _default;