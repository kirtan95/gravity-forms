"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _toConsumableArray2 = _interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));

var _validation = require("../validation");

var _checkConditionalLogic = _interopRequireDefault(require("./checkConditionalLogic"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var _default = function _default(field, event, inputID, formValues, setFormValues, conditionalIds, conditionFields) {
  for (var _len = arguments.length, props = new Array(_len > 7 ? _len - 7 : 0), _key = 7; _key < _len; _key++) {
    props[_key - 7] = arguments[_key];
  }

  var onChange = props.onChange;
  var id = field.id,
      type = field.type,
      isRequired = field.isRequired;
  var value;

  if (field.type === 'checkbox') {
    var values = (0, _toConsumableArray2["default"])(formValues[field.id].value);
    var index = values.indexOf(event.target.value);

    if (index > -1) {
      values.splice(index, 1);
    } else {
      values.push(event.target.value);
    }

    value = values;
  } else if (field.type == 'date' && field.dateType !== 'datepicker') {
    var subId = field.subId,
        dateLabel = field.dateLabel;

    var _values = (0, _toConsumableArray2["default"])(formValues[field.id].value);

    _values[subId] = {
      val: event.target.value,
      label: dateLabel
    };
    value = _values;
  } else if (field.type === 'consent') {
    value = event.target ? event.target.checked : 'null';
  } else if (field.type === 'address') {
    var _values2 = _objectSpread({}, formValues[field.id].value);

    if (inputID) {
      _values2[inputID] = event.target.value;
    }

    value = _values2;
  } else if (field.type === 'postcode') {
    value = event.target ? event.target.value : null;
    Object.values(formValues).filter(function (item) {
      return item.cssClass === 'field--street';
    })[0].value = event === null || event === void 0 ? void 0 : event.street;
    Object.values(formValues).filter(function (item) {
      return item.cssClass === 'field--city';
    })[0].value = event === null || event === void 0 ? void 0 : event.city;
  } else if (field.type === 'name') {
    var _values3 = formValues[field.id].value || {};

    var key = '-1' === inputID ? 'prefix' : event.target.getAttribute('data-key');
    _values3[key] = '-1' === inputID ? event.value : event.target.value;
    value = _values3;
  } else if (field.type === 'password' || field.type === 'email' && field.emailConfirmEnabled) {
    var _subId = field.subId;

    var _values4 = formValues[field.id] && formValues[field.id].value ? (0, _toConsumableArray2["default"])(formValues[field.id].value) : [];

    _values4[_subId] = {
      val: event.target.value
    };
    value = _values4;
  } else {
    value = event.target ? event.target.value : 'null';
  }

  if (type === 'text' && field.cssClass.indexOf('iban') > -1) {
    type = 'iban';
  }

  console.log('value', value);
  var valid = (0, _validation.validateField)(value, field);

  if (conditionalIds.indexOf(id) !== -1) {
    formValues[id].value = value;

    for (var i = 0; i < conditionFields.length; i++) {
      var _id = conditionFields[i].id;
      var hide = (0, _checkConditionalLogic["default"])(conditionFields[i].conditionalLogic, formValues);
      formValues[_id].hideField = hide;

      if (hide) {
        if (formValues[_id].isRequired && hide) {
          formValues[_id].value = '';
        }

        formValues[_id].valid = !!formValues[_id].isRequired;
      }
    }
  }

  var newValues = _objectSpread(_objectSpread({}, formValues), {}, (0, _defineProperty2["default"])({}, id, {
    value: value,
    id: id,
    valid: valid,
    type: field.type,
    label: field.label,
    pageNumber: field.pageNumber,
    cssClass: field.cssClass,
    isRequired: field.isRequired
  }));

  setFormValues(_objectSpread({}, newValues));

  if (onChange) {
    onChange(newValues);
  }
};

exports["default"] = _default;