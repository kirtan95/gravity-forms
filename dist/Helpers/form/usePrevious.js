"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = require("react");

var _default = function _default(data) {
  var ref = (0, _react.useRef)();
  (0, _react.useEffect)(function () {
    ref.current = data;
  }, [data]);
  return ref.current;
};

exports["default"] = _default;