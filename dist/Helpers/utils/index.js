"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "equalShallow", {
  enumerable: true,
  get: function get() {
    return _equalShallow["default"];
  }
});

var _equalShallow = _interopRequireDefault(require("./equalShallow"));