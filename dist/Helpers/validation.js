"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateField = exports.isRequired = exports.isEmail = exports.isUrl = exports.checkboxValidation = exports.selectValidation = exports.isEmpty = void 0;

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var getMessage = function getMessage(message, type) {
  if (!type) return false;

  if (message && (0, _typeof2["default"])(message) === 'object' && message.custom) {
    return message.custom;
  }

  if (message && (0, _typeof2["default"])(message) === 'object' && message[type]) {
    return message[type];
  }

  return message;
};

var isEmail = function isEmail(email, field, message) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (!re.test(email)) {
    var customMessage = getMessage(message, 'email');
    return customMessage || "Enter a valid email";
  }

  return false;
};

exports.isEmail = isEmail;

var isUrl = function isUrl(str, field, message) {
  var pattern = new RegExp('^(https?:\\/\\/)?' + '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + '((\\d{1,3}\\.){3}\\d{1,3}))' + '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + '(\\?[;&a-z\\d%_.~+=-]*)?' + '(\\#[-a-z\\d_]*)?$', 'i');

  if (!pattern.test(str)) {
    var customMessage = getMessage(message, 'url');
    return customMessage || "Enter a valid url";
  }

  return false;
};

exports.isUrl = isUrl;

var isEmpty = function isEmpty(value) {
  if (!value) {
    return true;
  }

  return false;
};

exports.isEmpty = isEmpty;

var isRequired = function isRequired(required, empty, message) {
  if (required && empty) {
    var customMessage = getMessage(message, 'required');
    return customMessage || 'This field is required';
  }

  return false;
};

exports.isRequired = isRequired;

var selectValidation = function selectValidation(required, value, placeholder) {
  return !(value === placeholder && required);
};

exports.selectValidation = selectValidation;

var checkboxValidation = function checkboxValidation(values, message) {
  if (values.length < 1) {
    return message || 'This field is required';
  }

  return false;
};

exports.checkboxValidation = checkboxValidation;

var emailValidation = function emailValidation(values, field) {
  var _ref = field || false,
      inputs = _ref.inputs,
      required = _ref.isRequired,
      errorMessage = _ref.errorMessage;

  var _ref2 = errorMessage || false,
      requiredMsg = _ref2.required,
      mismatch = _ref2.mismatch;

  var isInputsEmpty = values && values.filter(function (item) {
    return item && item.val === '';
  }).length;

  if ((values && values.length < 2 || isInputsEmpty !== 0) && required) {
    return requiredMsg || errorMessage || 'This field is required';
  }

  if (values && values.length > 0) {
    for (var index in values) {
      var isInValidMail = isEmail(values[index].val);

      if (isInValidMail) {
        return isInValidMail;
      }
    }
  }

  if (values && values.length === 2 && inputs && inputs.length === 2) {
    if (values[1] && values[0] && values[1].val !== '' && values[1].val !== values[0].val) {
      return mismatch || 'Email and Confirm Email does not match';
    }
  }

  return false;
};

var passwordValidation = function passwordValidation(values, field) {
  var _ref3 = field || false,
      inputs = _ref3.inputs,
      required = _ref3.isRequired,
      errorMessage = _ref3.errorMessage;

  var _ref4 = errorMessage || false,
      requiredMsg = _ref4.required,
      mismatch = _ref4.mismatch;

  var filteredInputs = inputs.filter(function (item) {
    return !item.isHidden;
  });
  var isInputsEmpty = values && values.filter(function (item) {
    return item && item.val === '';
  }).length;

  if ((values && values.length === 0 || isInputsEmpty === filteredInputs.length) && required) {
    return requiredMsg || errorMessage || 'This field is required';
  }

  if (values && values.length === 2 && filteredInputs && filteredInputs.length === 2) {
    if (values[1] && values[0] && values[1].val !== '' && values[1].val !== values[0].val) {
      return mismatch || 'Mismatch';
    }
  }

  return false;
};

var isPostcode = function isPostcode(postcode, field, message) {
  var re = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;

  if (!re.test(postcode)) {
    var customMessage = getMessage(message, 'email');
    return customMessage || "Enter a valid postcode";
  }

  return false;
};

var isDate = function isDate(values, field) {
  var validation = [];

  for (var i = 0; i < values.length; i++) {
    if (values[i]) {
      var _values$i = values[i],
          val = _values$i.val,
          label = _values$i.label;

      if (val) {
        if (label === 'MM') {
          var message = field.errorMessage['month'];
          var max = 12;
          var min = 1;
          var maxLength = 2;

          if (val.length > maxLength || val < min || val > max) {
            validation[i] = {
              index: i,
              message: getMessage(message, 'date') || "Enter a valid month"
            };
          }
        } else if (label === 'DD') {
          var _message = field.errorMessage['date'];
          var _max = 31;
          var _min = 1;
          var _maxLength = 2;

          if (val.length > _maxLength || val < _min || val > _max) {
            validation[i] = {
              index: i,
              message: getMessage(_message, 'date') || "Enter a valid date"
            };
          }
        } else if (label === 'YYYY') {
          var _message2 = field.errorMessage['year'];

          var _max2 = new Date().getFullYear() + 1;

          var _min2 = 1920;
          var _maxLength2 = 4;

          if (val.length > _maxLength2 || val < _min2 || val > _max2) {
            validation[i] = {
              index: i,
              message: getMessage(_message2, 'date') || "Enter a valid year"
            };
          }
        }
      }
    }
  }

  return validation;
};

var validateField = function validateField(value, field) {
  var type = field.type,
      required = field.isRequired;

  if ((type === 'checkbox' || type === 'radio') && required) {
    return checkboxValidation(value, field.errorMessage);
  }

  if (type === 'password') {
    return passwordValidation(value, field);
  }

  if (type === 'email' && field.emailConfirmEnabled) {
    return emailValidation(value, field);
  }

  var empty = isEmpty(value);
  var validationMessage = '';
  var message = field && field.errorMessage ? field.errorMessage : false;
  validationMessage = required ? isRequired(required, empty, message) : false;

  if (!validationMessage && !empty) {
    if (type === 'email') {
      validationMessage = isEmail(value, field, message);
    } else if (type === 'website') {
      validationMessage = isUrl(value, field, message);
    } else if (type === 'date') {
      var isValid = true;

      if (field.dateType && field.dateType === 'datepicker') {
        isValid = required ? isRequired(required, empty, message) : false;
      } else {
        isValid = isDate(value, field);
      }

      validationMessage = isValid.length > 0 ? isValid : false;
    } else if (type === 'postcode') {
      validationMessage = isPostcode(value, field, message);
    }
  }

  return validationMessage;
};

exports.validateField = validateField;