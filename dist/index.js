"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "FormConfirmation", {
  enumerable: true,
  get: function get() {
    return _FormElements.FormConfirmation;
  }
});
Object.defineProperty(exports, "FormError", {
  enumerable: true,
  get: function get() {
    return _FormElements.FormError;
  }
});
Object.defineProperty(exports, "RenderFields", {
  enumerable: true,
  get: function get() {
    return _FormElements.RenderFields;
  }
});
Object.defineProperty(exports, "Submit", {
  enumerable: true,
  get: function get() {
    return _FormElements.Submit;
  }
});
Object.defineProperty(exports, "validateField", {
  enumerable: true,
  get: function get() {
    return _validation.validateField;
  }
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _taggedTemplateLiteral2 = _interopRequireDefault(require("@babel/runtime/helpers/taggedTemplateLiteral"));

var _typeof2 = _interopRequireDefault(require("@babel/runtime/helpers/typeof"));

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _slicedToArray2 = _interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));

var _react = _interopRequireWildcard(require("react"));

var _isomorphicUnfetch = _interopRequireDefault(require("isomorphic-unfetch"));

var _FormElements = require("./FormElements");

var _client = require("@apollo/client");

var _form = require("./Helpers/form");

var _validation = require("./Helpers/validation");

function _templateObject() {
  var data = (0, _taggedTemplateLiteral2["default"])(["\n  mutation submitForm($formId: ID!, $fieldValues: [FormFieldValuesInput]!) {\n    submitGfForm(input: {id: $formId, fieldValues: $fieldValues}) {\n      errors {\n        id\n        message\n      }\n      entry {\n        createdById\n        id\n        isSubmitted\n        form {\n          confirmations {\n            message\n          }\n        }\n      }\n    }\n  }\n  "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var GravityForm = function GravityForm(props) {
  var _formData$pagination;

  var initialPage = props.initialPage,
      populatedEntry = props.populatedEntry,
      onChange = props.onChange,
      backendUrl = props.backendUrl,
      apiType = props.apiType;
  var client = apiType === 'graphql' ? new _client.ApolloClient({
    uri: backendUrl,
    cache: new _client.InMemoryCache(),
    fetchOptions: {
      mode: 'no-cors'
    },
    connectToDevTools: true
  }) : null;

  var _useState = (0, _react.useState)(false),
      _useState2 = (0, _slicedToArray2["default"])(_useState, 2),
      submitFailed = _useState2[0],
      setSubmitFailed = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = (0, _slicedToArray2["default"])(_useState3, 2),
      errorMessages = _useState4[0],
      setErrorMessages = _useState4[1];

  var _useState5 = (0, _react.useState)({}),
      _useState6 = (0, _slicedToArray2["default"])(_useState5, 2),
      formValues = _useState6[0],
      setFormValues = _useState6[1];

  var _useState7 = (0, _react.useState)(false),
      _useState8 = (0, _slicedToArray2["default"])(_useState7, 2),
      submitting = _useState8[0],
      setSubmitting = _useState8[1];

  var _useState9 = (0, _react.useState)(false),
      _useState10 = (0, _slicedToArray2["default"])(_useState9, 2),
      submitSuccess = _useState10[0],
      setSubmitSuccess = _useState10[1];

  var _useState11 = (0, _react.useState)(false),
      _useState12 = (0, _slicedToArray2["default"])(_useState11, 2),
      confirmationMessage = _useState12[0],
      setConfirmationMessage = _useState12[1];

  var _useState13 = (0, _react.useState)({}),
      _useState14 = (0, _slicedToArray2["default"])(_useState13, 2),
      formData = _useState14[0],
      setFormData = _useState14[1];

  var _useState15 = (0, _react.useState)({}),
      _useState16 = (0, _slicedToArray2["default"])(_useState15, 2),
      touched = _useState16[0],
      _setTouched = _useState16[1];

  var _useState17 = (0, _react.useState)(initialPage || 1),
      _useState18 = (0, _slicedToArray2["default"])(_useState17, 2),
      activePage = _useState18[0],
      setActivePage = _useState18[1];

  var _useState19 = (0, _react.useState)({}),
      _useState20 = (0, _slicedToArray2["default"])(_useState19, 2),
      conditionFields = _useState20[0],
      setConditionFields = _useState20[1];

  var _useState21 = (0, _react.useState)({}),
      _useState22 = (0, _slicedToArray2["default"])(_useState21, 2),
      conditionalIds = _useState22[0],
      setConditionalIds = _useState22[1];

  var _useState23 = (0, _react.useState)(false),
      _useState24 = (0, _slicedToArray2["default"])(_useState23, 2),
      isMultipart = _useState24[0],
      setIsMultiPart = _useState24[1];

  var _useState25 = (0, _react.useState)(false),
      _useState26 = (0, _slicedToArray2["default"])(_useState25, 2),
      pageClicked = _useState26[0],
      setPageClicked = _useState26[1];

  var _useState27 = (0, _react.useState)(false),
      _useState28 = (0, _slicedToArray2["default"])(_useState27, 2),
      showPageValidationMsg = _useState28[0],
      setShowPageValidationMsg = _useState28[1];

  var _useState29 = (0, _react.useState)({}),
      _useState30 = (0, _slicedToArray2["default"])(_useState29, 2),
      pages = _useState30[0],
      setPages = _useState30[1];

  var wrapperRef = (0, _react.useRef)(null);
  var updateEntryFields = (0, _react.useCallback)(function (populatedEntry) {
    (0, _form.updateFieldsValuesBasedOnEntry)(populatedEntry);
  }, []);
  (0, _react.useEffect)(function () {
    (0, _form.fetchForm)(_objectSpread({
      client: client,
      setFormData: setFormData,
      setFormValues: setFormValues,
      setActivePage: setActivePage,
      setConditionFields: setConditionFields,
      setConditionalIds: setConditionalIds,
      setPages: setPages,
      setIsMultiPart: setIsMultiPart
    }, props));
  }, []);
  (0, _react.useEffect)(function () {
    if (onChange) {
      onChange(formValues);
    }
  }, [formValues]);
  (0, _react.useEffect)(function () {
    if (populatedEntry) {
      updateEntryFields(populatedEntry);
    }
  }, [populatedEntry]);

  var scrollToFirstInvalidField = function scrollToFirstInvalidField() {
    if (!wrapperRef) return;
    var firstErrEl = wrapperRef.current.querySelector('.form-field.error');

    if (firstErrEl) {
      firstErrEl.scrollIntoView();
    }
  };

  var title = props.title,
      formID = props.formID,
      submitIcon = props.submitIcon,
      saveStateToHtmlField = props.saveStateToHtmlField,
      styledComponents = props.styledComponents,
      customComponents = props.customComponents,
      errorMessage = props.errorMessage,
      dropzoneText = props.dropzoneText,
      loadingSpinner = props.loadingSpinner,
      onError = props.onError,
      language = props.language,
      apiKeys = props.apiKeys;

  var _ref = styledComponents || false,
      Button = _ref.Button,
      Loading = _ref.Loading,
      SFormError = _ref.FormError,
      SFormConfirmation = _ref.FormConfirmation,
      _ref$GFWrapper = _ref.GFWrapper,
      GFWrapper = _ref$GFWrapper === void 0 ? 'div' : _ref$GFWrapper;

  var cssClass = formData.cssClass;

  var handlePrevStep = function handlePrevStep(e) {
    e.preventDefault();
    (0, _form.prevStep)(formValues, pages, activePage, setActivePage, setPageClicked);
  };

  var _onSubmit = function () {
    var _ref2 = (0, _asyncToGenerator2["default"])(_regenerator["default"].mark(function _callee(event, formDataState, formValuesState) {
      var customOnSubmit, filterFormData, formData, isFormValid;
      return _regenerator["default"].wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              customOnSubmit = props.onSubmit, filterFormData = props.filterFormData;
              formData = new FormData(event.target);
              if (filterFormData) formData = filterFormData(formData, formValues);
              event.preventDefault();
              isFormValid = (0, _form.forceValidation)(activePage, formValues, setShowPageValidationMsg, _setTouched);

              if (isFormValid) {
                _context.next = 8;
                break;
              }

              scrollToFirstInvalidField();
              return _context.abrupt("return", false);

            case 8:
              if (customOnSubmit) {
                customOnSubmit(formData);
              } else {
                setSubmitting(true);
                setSubmitSuccess(false);
                setSubmitFailed(false);
                setConfirmationMessage(false);
                setErrorMessages(false);

                if (apiType === 'graphql') {
                  submitWithGraphQLApi(props, formDataState, formValuesState, client, setSubmitting, setSubmitSuccess, setConfirmationMessage, _form.scrollToConfirmation, setSubmitFailed, setErrorMessages);
                } else {
                  submitWithRestApi(props, formData, setSubmitting, setSubmitSuccess, setConfirmationMessage, _form.scrollToConfirmation, setSubmitFailed, setErrorMessages);
                }
              }

            case 9:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function onSubmit(_x, _x2, _x3) {
      return _ref2.apply(this, arguments);
    };
  }();

  var gfwrapper = _react["default"].createElement(GFWrapper, {
    ref: wrapperRef,
    className: "form-wrapper",
    id: "gravity_form_".concat(formID)
  }, formData.title ? null : Loading && _react["default"].createElement(Loading, {
    isLoading: true
  }), submitFailed && !submitSuccess && !onError && _react["default"].createElement(_FormElements.FormError, {
    SFormError: SFormError || false,
    errorMessage: errorMessage || 'There was a problem with your submission'
  }), submitSuccess && confirmationMessage && _react["default"].createElement(_FormElements.FormConfirmation, {
    confirmation: confirmationMessage,
    SFormConfirmation: SFormConfirmation
  }), !submitSuccess && formData.fields ? _react["default"].createElement("form", {
    onSubmit: function onSubmit(event) {
      return _onSubmit(event, formData, formValues);
    },
    className: cssClass,
    encType: isMultipart ? 'multipart/form-data' : undefined,
    noValidate: true
  }, (formData.title || formData.description) && _react["default"].createElement("div", {
    "class": "form-heading"
  }, formData.title && title ? _react["default"].createElement("h3", {
    className: "form-title"
  }, formData.title) : null, formData.description ? _react["default"].createElement("p", {
    className: "form-description"
  }, formData.description) : null), _react["default"].createElement("div", {
    className: "form-wrapper"
  }, (formData === null || formData === void 0 ? void 0 : (_formData$pagination = formData.pagination) === null || _formData$pagination === void 0 ? void 0 : _formData$pagination.pages) && _react["default"].createElement(_FormElements.ProgressBar, {
    pagination: formData.pagination,
    activePage: activePage,
    firstPageCssClass: formData.firstPageCssClass
  }), _react["default"].createElement(_FormElements.RenderFields, (0, _extends2["default"])({
    styledComponents: styledComponents,
    customComponents: customComponents,
    fields: formData.fields,
    formValues: formValues,
    submitFailed: submitFailed,
    submitSuccess: submitSuccess,
    updateForm: function updateForm(event, field, inputID) {
      return (0, _form.updateFormHandler)(field, event, inputID, formValues, setFormValues, conditionalIds, conditionFields);
    },
    touched: touched,
    setTouched: function setTouched(id) {
      return (0, _form.setTouchedHandler)(id, touched, _setTouched);
    },
    setErrorMessages: setErrorMessages,
    pagination: formData.pagination,
    activePage: activePage,
    prevStep: function prevStep(e) {
      return handlePrevStep(e);
    },
    nextStep: function nextStep(e) {
      return (0, _form.nextStep)(e, props, pages, formValues, activePage, setActivePage, setPageClicked, _setTouched, setShowPageValidationMsg);
    },
    checkConditionalLogic: function checkConditionalLogic(condition, fields) {
      return (0, _form.checkConditionalLogic)(condition, fields = false);
    },
    saveStateToHtmlField: saveStateToHtmlField,
    enableHoneypot: formData.enableHoneypot,
    errors: errorMessages,
    unsetError: function unsetError(id) {
      return (0, _form.unsetError)(id, errorMessages);
    },
    dropzoneText: dropzoneText,
    pageClicked: pageClicked,
    language: language,
    apiKeys: apiKeys
  }, props)), (!formData.pagination || formData.pagination && formData.pagination.pages.length === activePage) && _react["default"].createElement(_FormElements.Submit, {
    Button: Button,
    Loading: Loading,
    formData: formData,
    submitIcon: submitIcon,
    submitting: submitting,
    prevStep: function prevStep(e) {
      return handlePrevStep(e);
    },
    loadingSpinner: loadingSpinner
  }))) : '');

  return apiType === 'graphql' ? _react["default"].createElement(_client.ApolloProvider, {
    client: client
  }, gfwrapper) : gfwrapper;
};

function submitWithGraphQLApi(_x4, _x5, _x6, _x7, _x8, _x9, _x10, _x11, _x12, _x13) {
  return _submitWithGraphQLApi.apply(this, arguments);
}

function _submitWithGraphQLApi() {
  _submitWithGraphQLApi = (0, _asyncToGenerator2["default"])(_regenerator["default"].mark(function _callee2(props, formData, formValues, client, setSubmitting, setSubmitSuccess, setConfirmationMessage, scrollToConfirmation, setSubmitFailed, setErrorMessages) {
    var formID, jumpToConfirmation, onSubmitSuccess, onError, SUBMIT_FORM, fieldValues, result, confirmationMessage, errors, errorMessages;
    return _regenerator["default"].wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            formID = props.formID, jumpToConfirmation = props.jumpToConfirmation, onSubmitSuccess = props.onSubmitSuccess, onError = props.onError;
            SUBMIT_FORM = (0, _client.gql)(_templateObject());
            fieldValues = getFieldValues(formData, formValues);
            console.log({
              formID: formID,
              fieldValues: fieldValues
            });
            _context2.next = 6;
            return client.mutate({
              mutation: SUBMIT_FORM,
              variables: {
                formId: formID,
                fieldValues: fieldValues
              }
            });

          case 6:
            result = _context2.sent;

            if (result.data.submitGfForm.entry) {
              confirmationMessage = result.data.submitGfForm.entry.form.confirmations[0].message;
              setSubmitting(false);
              setSubmitSuccess(true);
              setConfirmationMessage(confirmationMessage);

              if (jumpToConfirmation) {
                scrollToConfirmation(props, wrapperRef, jumpToConfirmation);
              }
            } else {
              errors = result.data.submitGfForm.errors;
              errorMessages = {};
              errors.forEach(function (value) {
                errorMessages[value.id] = value.message;
              });

              if (onError) {
                onError(errorMessages);
                setSubmitting(false);
                setSubmitFailed(true);
              } else {
                setSubmitting(false);
                setSubmitFailed(true);
                setErrorMessages(errorMessages);
              }
            }

          case 8:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _submitWithGraphQLApi.apply(this, arguments);
}

function getFieldValues(formData, formValues) {
  var fieldValues = [];
  Object.keys(formValues).forEach(function (field, index) {
    field = formValues[field];

    if (field.type === 'name' && field.value) {
      fieldValues.push({
        id: field.id,
        nameValues: field.value
      });
    }

    if (['text', 'textarea'].includes(field.type) && field.value) {
      fieldValues.push({
        id: field.id,
        value: field.value
      });
    }

    if (field.type === 'multiselect' && field.value) {
      fieldValues.push({
        id: field.id,
        values: field.value.map(function (x) {
          return x.value;
        })
      });
    }

    if (field.type === 'email' && field.value) {
      var emailValues = {
        value: typeof field.value === 'string' ? field.value : field.value[0].val
      };

      if ((0, _typeof2["default"])(field.value) === 'object' && field.value.length > 1) {
        emailValues['confirmationValue'] = field.value[1].val;
      }

      fieldValues.push({
        id: field.id,
        emailValues: emailValues
      });
    }
  });
  return fieldValues;
}

function submitWithRestApi(props, formData, setSubmitting, setSubmitSuccess, setConfirmationMessage, scrollToConfirmation, setSubmitFailed, setErrorMessages) {
  var formID = props.formID,
      backendUrl = props.backendUrl,
      jumpToConfirmation = props.jumpToConfirmation,
      onSubmitSuccess = props.onSubmitSuccess,
      onError = props.onError;
  var gfSubmissionUrl = backendUrl.substring(0, backendUrl.indexOf('/wp-json'));
  (0, _isomorphicUnfetch["default"])("".concat(gfSubmissionUrl, "/wp-json/gf/v2/forms/").concat(formID, "/submissions"), {
    method: 'POST',
    body: formData
  }).then(function (resp) {
    return resp.json();
  }).then(function (response) {
    if (response && response.is_valid) {
      if (onSubmitSuccess) {
        var res = onSubmitSuccess(response);

        if (!res) {
          return false;
        }
      }

      var confirmationMessage = response.confirmation_message;

      var _ref3 = confirmationMessage || false,
          type = _ref3.type,
          link = _ref3.link;

      if (type && link && type === 'redirect') {
        if (typeof window !== 'undefined') {
          window.location.replace(link);
          return false;
        }
      }

      setSubmitting(false);
      setSubmitSuccess(true);
      setConfirmationMessage(confirmationMessage);

      if (jumpToConfirmation) {
        scrollToConfirmation(props, wrapperRef, jumpToConfirmation);
      }
    } else {
      throw {
        response: response
      };
    }
  })["catch"](function (error) {
    var errorMessages = error && error.response && error.response.validation_messages ? error.response.validation_messages : 'Something went wrong';

    if (onError) {
      onError(errorMessages);
      setSubmitting(false);
      setSubmitFailed(true);
    } else {
      setSubmitting(false);
      setSubmitFailed(true);
      setErrorMessages(errorMessages);
    }

    if (jumpToConfirmation) {
      scrollToConfirmation(props, wrapperRef);
    }
  });
}

GravityForm.defaultProps = {
  title: true,
  submitIcon: false,
  saveStateToHtmlField: false,
  jumpToConfirmation: true
};
var _default = GravityForm;
exports["default"] = _default;