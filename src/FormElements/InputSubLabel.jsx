import React from 'react';

const InputSubLabel = ({ formId, id, label, labelPlacement, styledComponent }) => {
  const { SubLabel = 'label' } = styledComponent || false;

  return (
    <SubLabel
      htmlFor={`input_${formId}_${id}`}
      className={`gf-label ${labelPlacement}`}
      style={{ display: labelPlacement === 'hidden_label' ? 'none' : undefined }}
    >
      {label}
    </SubLabel>
  );
};

export default InputSubLabel;
