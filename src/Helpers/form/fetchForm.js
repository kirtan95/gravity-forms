import fetch from 'isomorphic-unfetch';
import { getFieldPrepopulatedValue } from './index';
import { validateField } from '../validation';
import checkConditionalLogic from './checkConditionalLogic';
import { gql } from '@apollo/client';

async function fetchFromRestEndpoint(getParams, backendUrl, formID, fetchOptions) {
  const queryString = getParams
    ? Object.keys(getParams)
      .map((key) => `${key}=${getParams[key]}`)
      .join('&')
    : '';
  const requestUrl = `${backendUrl}/${formID}${queryString ? `?${queryString}` : ''}`;

  const form = await fetch(requestUrl, fetchOptions)
    .then((resp) => resp.json())
    .then((response) => response)
    .catch(() => false);

  return form;
}

async function fetchFromGraphQLEndpoint(client, getParams, backendUrl, formID, fetchOptions) {
  const GET_FORM = gql`
  query getForm($formId: ID!) {
    gfForm(id: $formId, idType: DATABASE_ID) {
      id
      title
      description
      submitButton {
        text
        imageUrl
        width
      }
      confirmations {
        isDefault
        message
      }
      pagination {
        type
        style
        progressbarCompletionText
        pageNames
        hasProgressbarOnConfirmation
        color
        backgroundColor
        lastPageButton {
          imageUrl
          text
          type
        }
      }
      formFields(first: 500) {
        nodes {
          id
          type
          pageNumber
          ... on AddressField {
            ...AddressFieldFields
          }
          ... on CheckboxField {
            ...CheckboxFieldFields
          }
          ... on ConsentField {
            ...ConsentFieldFields
          }
          ... on DateField {
            ...DateFieldFields
          }
          ... on EmailField {
            ...EmailFieldFields
          }
          ... on FileUploadField {
            ...FileUploadFieldFields
          }
          ... on HiddenField {
            ...HiddenFieldFields
          }
          ... on HtmlField {
            ...HtmlFieldFields
          }
          ... on MultiSelectField {
            ...MultiSelectFieldFields
          }
          ... on NameField {
            ...NameFieldFields
          }
          ... on NumberField {
            ...NumberFieldFields
          }
          ... on PageField {
            ...PageFieldFields
          }
          ... on PasswordField {
            ...PasswordFieldFields
          }
          ... on PhoneField {
            ...PhoneFieldFields
          }
          ... on RadioField {
            ...RadioFieldFields
          }
          ... on SectionField {
            ...SectionFieldFields
          }
          ... on SelectField {
            ...SelectFieldFields
          }
          ... on TextField {
            ...TextFieldFields
          }
          ... on TextAreaField {
            ...TextAreaFieldFields
          }
          ... on TimeField {
            ...TimeFieldFields
          }
          ... on WebsiteField {
            ...WebsiteFieldFields
          }
        }
      }
      formId
      databaseId
      firstPageCssClass
      hasHoneypot
      button {
        text
        type
        location
        imageUrl
        width
        conditionalLogic {
          actionType
          logicType
          rules {
            fieldId
            operator
            value
          }
        }
      }
      labelPlacement
      descriptionPlacement
    }
  }
  
  fragment AddressFieldFields on AddressField {
    label
    description
    cssClass
    inputName
    inputs {
      defaultValue
      autocompleteAttribute
      customLabel
      id
      isHidden
      key
      label
      name
      placeholder
    }
    conditionalLogic {
      rules {
        fieldId
        operator
        value
      }
      actionType
      logicType
    }
    inputs {
      defaultValue
      autocompleteAttribute
      customLabel
      id
      isHidden
      key
      label
      name
      placeholder
    }
    errorMessage
    isRequired
    descriptionPlacement
    labelPlacement
    pageNumber
    value
  }
  
  fragment CheckboxFieldFields on CheckboxField {
    inputName
    label
    isRequired
    description
    cssClass
    conditionalLogic {
      rules {
        fieldId
        operator
        value
      }
      actionType
      logicType
    }
    inputs {
      id
      label
      name
    }
    choices {
      text
      value
      isSelected
    }
    errorMessage
    descriptionPlacement
    labelPlacement
    pageNumber
    value
  }
  
  fragment ConsentFieldFields on ConsentField {
    label
    cssClass
    conditionalLogic {
      actionType
      logicType
      rules {
        fieldId
        operator
        value
      }
    }
    isRequired
    description
    descriptionPlacement
    labelPlacement
    checkboxLabel
    pageNumber
    errorMessage
    value
  }
  
  fragment DateFieldFields on DateField {
    inputName
    label
    description
    cssClass
    isRequired
    placeholder
    conditionalLogic {
      rules {
        fieldId
        operator
        value
      }
      actionType
      logicType
    }
    defaultValue
    errorMessage
    inputs {
      autocompleteAttribute
      customLabel
      defaultValue
      id
      label
      placeholder
    }
    dateType
    descriptionPlacement
    labelPlacement
    pageNumber
    dateFormat
    value
  }
  
  fragment EmailFieldFields on EmailField {
    label
    description
    cssClass
    isRequired
    placeholder
    conditionalLogic {
      rules {
        fieldId
        operator
        value
      }
      actionType
      logicType
    }
    errorMessage
    hasEmailConfirmation
    inputs {
      autocompleteAttribute
      customLabel
      defaultValue
      id
      label
      name
      placeholder
    }
    descriptionPlacement
    labelPlacement
    pageNumber
    value
  }
  
  fragment FileUploadFieldFields on FileUploadField {
    conditionalLogic {
      actionType
      logicType
      rules {
        fieldId
        value
        operator
      }
    }
    cssClass
    description
    allowedExtensions
    descriptionPlacement
    errorMessage
    isRequired
    label
    labelPlacement
    maxFileSize
    maxFiles
    pageNumber
    value
    values
  }
  
  fragment HiddenFieldFields on HiddenField {
    label
    pageNumber
    inputName
    defaultValue
    value
  }
  
  fragment HtmlFieldFields on HtmlField {
    label
    pageNumber
    content
    cssClass
    conditionalLogic {
      actionType
      logicType
      rules {
        fieldId
        operator
        value
      }
    }
  }
  
  fragment MultiSelectFieldFields on MultiSelectField {
    inputName
    label
    description
    cssClass
    isRequired
    choices {
      text
      value
      isSelected
    }
    conditionalLogic {
      rules {
        fieldId
        operator
        value
      }
      actionType
      logicType
    }
    errorMessage
    descriptionPlacement
    labelPlacement
    pageNumber
    value
    values
  }
  
  fragment NameFieldFields on NameField {
    inputName
    label
    description
    cssClass
    isRequired
    inputs {
      key
      label
      placeholder
      choices {
        text
        value
        isSelected
      }
      defaultValue
      autocompleteAttribute
      customLabel
      hasChoiceValue
      id
      isHidden
      name
    }
    conditionalLogic {
      rules {
        fieldId
        operator
        value
      }
      actionType
      logicType
    }
    errorMessage
    descriptionPlacement
    labelPlacement
    pageNumber
    value
    nameValues {
      first
      last
      middle
      prefix
      suffix
    }
  }
  
  fragment NumberFieldFields on NumberField {
    label
    labelPlacement
    placeholder
    pageNumber
    isRequired
    description
    descriptionPlacement
    cssClass
    conditionalLogic {
      actionType
      logicType
      rules {
        fieldId
        operator
        value
      }
    }
    inputName
    defaultValue
    errorMessage
    value
  }
  
  fragment PageFieldFields on PageField {
    conditionalLogic {
      actionType
      logicType
      rules {
        fieldId
        operator
        value
      }
    }
    cssClass
    pageNumber
    nextButton {
      imageUrl
      text
      type
      conditionalLogic {
        actionType
        logicType
        rules {
          fieldId
          operator
          value
        }
      }
    }
    previousButton {
      imageUrl
      text
      type
      conditionalLogic {
        actionType
        logicType
        rules {
          fieldId
          operator
          value
        }
      }
    }
  }
  
  fragment PasswordFieldFields on PasswordField {
    conditionalLogic {
      actionType
      logicType
      rules {
        fieldId
        operator
        value
      }
    }
    cssClass
    description
    descriptionPlacement
    errorMessage
    inputs {
      customLabel
      id
      isHidden
      label
      placeholder
    }
    label
    isRequired
    labelPlacement
    pageNumber
    hasPasswordStrengthIndicator
    minPasswordStrength
    value
  }
  
  fragment PhoneFieldFields on PhoneField {
    inputName
    label
    description
    cssClass
    isRequired
    placeholder
    conditionalLogic {
      rules {
        fieldId
        operator
        value
      }
      actionType
      logicType
    }
    defaultValue
    errorMessage
    descriptionPlacement
    labelPlacement
    pageNumber
    value
  }
  
  fragment RadioFieldFields on RadioField {
    inputName
    label
    isRequired
    description
    cssClass
    choices {
      text
      value
      isSelected
      isOtherChoice
    }
    conditionalLogic {
      rules {
        fieldId
        operator
        value
      }
      actionType
      logicType
    }
    errorMessage
    descriptionPlacement
    labelPlacement
    pageNumber
    hasOtherChoice
    value
  }
  
  fragment SectionFieldFields on SectionField {
    cssClass
    description
    label
    pageNumber
    conditionalLogic {
      actionType
      logicType
      rules {
        fieldId
        operator
        value
      }
    }
  }
  
  fragment SelectFieldFields on SelectField {
    label
    description
    cssClass
    isRequired
    defaultValue
    choices {
      text
      value
      isSelected
    }
    conditionalLogic {
      rules {
        fieldId
        operator
        value
      }
      actionType
      logicType
    }
    inputName
    errorMessage
    placeholder
    descriptionPlacement
    labelPlacement
    pageNumber
    value
  }
  
  fragment TextAreaFieldFields on TextAreaField {
    label
    description
    cssClass
    isRequired
    conditionalLogic {
      rules {
        fieldId
        operator
        value
      }
      actionType
      logicType
    }
    defaultValue
    inputName
    errorMessage
    placeholder
    maxLength
    descriptionPlacement
    labelPlacement
    pageNumber
    value
  }
  
  fragment TextFieldFields on TextField {
    inputName
    label
    description
    cssClass
    isRequired
    placeholder
    conditionalLogic {
      rules {
        fieldId
        operator
        value
      }
      actionType
      logicType
    }
    defaultValue
    errorMessage
    maxLength
    descriptionPlacement
    labelPlacement
    pageNumber
    value
  }
  
  fragment TimeFieldFields on TimeField {
    label
    description
    cssClass
    isRequired
    conditionalLogic {
      rules {
        fieldId
        operator
        value
      }
      actionType
      logicType
    }
    errorMessage
    inputs {
      id
      label
      placeholder
      defaultValue
      customLabel
      autocompleteAttribute
    }
    descriptionPlacement
    labelPlacement
    pageNumber
    value
  }
  
  fragment WebsiteFieldFields on WebsiteField {
    inputName
    label
    description
    cssClass
    isRequired
    placeholder
    conditionalLogic {
      rules {
        fieldId
        operator
        value
      }
      actionType
      logicType
    }
    defaultValue
    errorMessage
    descriptionPlacement
    labelPlacement
    pageNumber
    value
  }
`;
  const result = await client
    .query({
      query: GET_FORM,
      variables: { formId: formID },
    });

  return mapGraphQLResponsetoREST(result?.data?.gfForm);

  return false
}

function mapGraphQLResponsetoREST(data) {
  if (!data) return data

  data = {...data, formId: data.databaseId}
  
  data.fields = data.formFields.nodes.map(field => {
    field = {...field,type: field.type.toLowerCase()}
    
    if (field?.cssClass === null) {
      field.cssClass = ""
    }
    if (field?.defaultValue === null) {
      field.defaultValue = ""
    }
    if (field?.inputName === null) {
      field.inputName = ""
    }
    if (field?.placeholder === null) {
      field.placeholder = ""
    }
    if (field?.conditionalLogic === null) {
      field.conditionalLogic = ""
    }
    if (field?.value === null) {
      field.value = ""
    }
    if (field?.defaultValue === null) {
      field.defaultValue = ""
    }
    if (field?.errorMessage === null) {
      field.errorMessage = ""
    }
    if (field?.descriptionPlacement === "INHERIT") {
      field.descriptionPlacement = ""
    }
    if(field?.maxLength === 0) {
      field.maxLength = ""
    }
    if ('hasPasswordStrengthIndicator' in field) {
      field.passwordStrengthEnabled = field.hasPasswordStrengthIndicator
    }
  
    if(field?.inputs) {
      field.inputs = field.inputs.map( input => {
        return {
          ...input, 
          cssClass: "", 
          defaultValue: input.defaultValue === null ? '' : input.defaultValue
        }
      })
    }
    if (field.type === 'name') {
      field.defaultValue = ""
      field.inputs = field.inputs.map( input => {
        if(input.choices !== null) {
          input.inputType = 'radio'
        }
        return input
      })
    }
    if (field.type === 'address') {
      field.placeholder = ""
      field.maxLength = ""
    }

    field.formId = data.databaseId

    if ('hasEmailConfirmation' in field) {
      field.emailConfirmEnabled = field.hasEmailConfirmation
    }

    return field
  })
  if (data?.pagination?.pageNames) {
    data.pagination = {...data.pagination, pages: data.pagination.pageNames}
  }
  if (!'maxLength' in data) {
    data.maxLength = ""
  }
  if ('hasHoneypot' in data) {
    data.enableHoneypot = data.hasHoneypot
  }

  return data
}

async function fetchForm({
  client,
  initialPage,
  fetchOptions,
  setFormData,
  setFormValues,
  setActivePage,
  setConditionFields,
  setConditionalIds,
  setPages,
  setIsMultiPart,
  populatedFields,
  populatedEntry,
  getParams,
  backendUrl,
  formID,
  apiType
}) {
  let isMultipart = false;
  const params = [getParams, backendUrl, formID, fetchOptions]
  // const form = await fetchFromRestEndpoint(...params)
  const form = apiType === 'graphql' ? await fetchFromGraphQLEndpoint(client, ...params) : await fetchFromRestEndpoint(...params)

  if (form) {
    const formValues = {};
    const conditionFields = [];
    const conditionalIds = [];
    const pages = [];
    // eslint-disable-next-line no-restricted-syntax
    for (const field of form.fields) {
      let value;

      if (field.type === 'page') {
        pages.push(field.id);
      }

      value = getFieldPrepopulatedValue(field, populatedFields, populatedEntry);

      if (field.type === 'fileupload') {
        isMultipart = true;
      }

      // grab all conditional logic fields
      if (field.conditionalLogic) {
        const tmpField = {
          id: field.id,
          conditionalLogic: field.conditionalLogic,
        };
        const ids = field.conditionalLogic.rules.map((item) => item.fieldId);
        for (let i = 0; i < ids.length; i++) {
          const id = parseInt(ids[i]);
          if (conditionalIds.indexOf(id) === -1) {
            conditionalIds.push(id);
          }
        }
        conditionFields.push(tmpField);
      }

      formValues[field.id] = {
        valid: validateField(value, field),
        value,
        type: field.type,
        label: field.label,
        pageNumber: field.pageNumber,
        cssClass: field.cssClass,
        isRequired: field.isRequired,
        type: field.type,
      };
    }

    // check condition logic
    for (let i = 0; i < conditionFields.length; i++) {
      formValues[conditionFields[i].id].hideField = checkConditionalLogic(
        conditionFields[i].conditionalLogic,
        formValues
      );
    }
    setFormData(form);
    setFormValues(formValues);
    setActivePage(initialPage || (form.pagination ? 1 : false));
    setConditionFields(conditionFields);
    setConditionalIds(conditionalIds);

    if (isMultipart) setIsMultiPart(isMultipart);

    if (!!pages.length) setPages(pages);
  }
}

export default fetchForm;
